package be.ac.ulb.infof307.g06.utils;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;


import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TestStringComparator extends StringComparator {
    String originalText;
    String invertedLine;
    String insertedLine;
    String deletedLine;

    @BeforeAll
    void setup(){
        originalText = "\\draw[color=0x000000ff,fill=0x000000ff] (160,222) circle (50pt);\n" +
                "\\draw[color=0xcc3333ff,fill=0xcc3333ff] (307,424) circle (50pt);\n" +
                "\\draw[color=0x000000ff,fill=0x000000ff] (411,247) circle (50pt);\n" +
                "\\draw[color=0x000000ff,->] (201,251) -- (421,272);";
        invertedLine = "\\draw[color=0x000000ff,fill=0x000000ff] (160,222) circle (50pt);\n" +
                "\\draw[color=0x000000ff,fill=0x000000ff] (411,247) circle (50pt);\n" +
                "\\draw[color=0x000000ff,->] (201,251) -- (421,272);\n" +
                "\\draw[color=0xcc3333ff,fill=0xcc3333ff] (307,424) circle (50pt);";
        insertedLine = "\\draw[color=0x000000ff,fill=0x000000ff] (160,222) circle (50pt);\n" +
                "\\draw[color=0xcc3333ff,fill=0xcc3333ff] (307,424) circle (50pt);\n" +
                "\\draw[color=0x000000ff,fill=0x000000ff] (411,247) circle (50pt);\n" +
                "\\draw[color=0x000000ff,->] (201,251) -- (421,272);\n" +
                "\\draw[color=0x000000ff,->] (201,251) -- (421,272);";
        deletedLine = "\\draw[color=0x000000ff,fill=0x000000ff] (160,222) circle (50pt);\n" +
                "\\draw[color=0xcc3333ff,fill=0xcc3333ff] (307,424) circle (50pt);\n" +
                "\\draw[color=0x000000ff,fill=0x000000ff] (411,247) circle (50pt);\n";
    }

    /**
     * Check if the content is the same and has no deletion
     */
    @Test
    public void doesSameStringGetDeletionEmptyReturnTrue(){
        assertTrue(StringComparator.getDeletions(originalText,originalText).isEmpty());
    }

    /**
     * Check if the content is the same and has no insertion
     */
    @Test
    public void doesSameStringGetInsertionEmptyReturnTrue(){
        assertTrue(StringComparator.getInsertions(originalText,originalText).isEmpty());;
    }

    /**
     * Check if content is the same
     */
    @Test
    public void hasSameStringTheSameCommonReturnTrue(){
        assertTrue(compareStrings(originalText,StringComparator.getCommon(originalText,originalText)));
    }

    /**
     * check if insertion is detected
     */
    @Test
    public void doesInsertedLineGetInsertionEmptyReturnFalse(){
        assertFalse(StringComparator.getInsertions(originalText,insertedLine).isEmpty());
    }

    /**
     * check if there is no deletion
     */
    @Test
    public void doesInsertedLineGetDeletionEmptyReturnTrue(){
        assertTrue(StringComparator.getDeletions(originalText,insertedLine).isEmpty());
    }

    /**
     * check if the inserted string has the same common lines
     */
    @Test
    public void hasInsertedStringTheSameCommonReturnTrue(){
        assertTrue(compareStrings(originalText,StringComparator.getCommon(originalText,insertedLine)));
    }

    /**
     * check if no insertion detected
     */
    @Test
    public void doesDeletedLineGetInsertionEmptyReturnTrue(){
        assertTrue(StringComparator.getInsertions(originalText,deletedLine).isEmpty());
    }

    /**
     * check if deletion detected
     */
    @Test
    public void doesDeletedLineGetDeletionEmptyReturnFalse() {
        assertFalse(StringComparator.getDeletions(originalText, deletedLine).isEmpty());
    }

    /**
     * check if deleted line string has the same common
     */
    @Test
    public void hasDeletedLineTheSameCommonReturnFalse() {
        assertFalse(compareStrings(originalText,StringComparator.getCommon(originalText,deletedLine)));
    }


    /**
     * detect if no insertion when line is just swapped
     */
    @Test
    public void doesInvertedLineGetInsertionEmptyReturnTrue(){
        assertTrue(StringComparator.getInsertions(originalText,invertedLine).isEmpty());
    }

    /**
     * check if no deletion if line just swapped
     */
    @Test
    public void doesInvertedLineGetDeletionEmptyReturnTrue() {
        assertTrue(StringComparator.getInsertions(originalText, invertedLine).isEmpty());
    }


    /**
     * check if same content as original
     */
    @Test
    public void hasInvertedLineTheSameCommonReturnTrue() {
        assertTrue(compareStrings(originalText, StringComparator.getCommon(originalText, invertedLine)));
    }

    /**
     * check if occurrence of "a" is equal to 3
     */
    @Test
    public void checkIfLetterAOccurrencesEquals3(){
        ArrayList<String> testArray = new ArrayList<>();
        testArray.add("a");
        testArray.add("a");
        testArray.add("a");
        assertEquals(3,getNumberOfOccurrence("a",testArray));
    }

    /**
     * test the split function
     */
    @Test
    public void checkIfTestArrayIsCorrectlySplit(){
        ArrayList<String> testArray = splitIntoArrayList(originalText);
        assertTrue(compareStrings(originalText,testArray));
    }

    /**
     * Check if the content Stays the same
     * @param originalText : the string of the first text
     * @param arrayList : an array containing the second text split by the new lines
     * @return  res : a boolean that indicate if the 2 texts are the sames
     */
    private boolean compareStrings(String originalText,ArrayList<String> arrayList){
        ArrayList<String> originalArray = splitIntoArrayList(originalText);
        for(String line : arrayList){
            originalArray.remove(line);
        }
        return originalArray.isEmpty();
    }




}
