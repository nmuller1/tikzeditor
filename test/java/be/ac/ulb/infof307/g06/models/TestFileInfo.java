package be.ac.ulb.infof307.g06.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestFileInfo {
    private FileInfo fi;
    @BeforeEach
    void setUp() {
        fi = new FileInfo("TestP","Admin","12.03.2020","C",1);
    }

    @Test
    void isProjectNameTestP(){
        assertEquals("TestP",fi.getProjectName());
    }

    @Test
    void changeProjectNameToP() {
        fi.setProjectName("P");
        assertEquals("P",fi.getProjectName());
    }

    @Test
    void isOwnerNameAdmin() {
        assertEquals("Admin",fi.getOwnerName());
    }

    @Test
    void setOwnerNameToJoJo() {
        fi.setOwnerName("JoJo");
        assertEquals("JoJo",fi.getOwnerName());
    }

    @Test
    void isLastModification12March2020() {
        assertEquals("12.03.2020",fi.getLastModification());
    }

    @Test
    void setLastModificationTo23March2020() {
        fi.setLastModification("23.03.2020");
        assertEquals("23.03.2020",fi.getLastModification());
    }

    @Test
    void isFilePathC(){
        assertEquals("C",fi.getFilePath());
    }

    @Test
    void setFilePathToD() {
        fi.setFilePath("D");
        assertEquals("D",fi.getFilePath());
    }

    @Test
    void isProjectId1() {
        assertEquals(1,fi.getProjectId());
    }

    @Test
    void setProjectIdTo2() {
        fi.setProjectId(2);
        assertEquals(2,fi.getProjectId());
    }
}