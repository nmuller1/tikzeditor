package be.ac.ulb.infof307.g06.models.versioning;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

/**
 * These are the tests for the CommitContent class.
 * They way this is being testes is that we are going to pass
 * a string to the class and in function of the return values
 * of the getters we can identify if the parseCommitContent
 * function works properly.
 */
class TestCommitModel {
    private CommitModel commitModel;

    TestCommitModel(){
        commitModel = new CommitModel("commit 0x10003413\n" +
                "\t+\\draw (1,2) circle (3cm);\n" +
                "\t-\\draw (12,13) -- (54,12);\n" +
                "\t+\\draw (12,13) rectangle (13, 14);\n\n");
    }

    @Test
    public void getNewLinesReturnsTwoAdditions(){
        ArrayList<String> expectedAdditions = new ArrayList<>(
                Arrays.asList("\\draw (1,2) circle (3cm);", "\\draw (12,13) rectangle (13, 14);"));
        assertEquals(expectedAdditions, commitModel.getNewLines());
    }

    @Test
    public void getRemovedLinesReturnsOneDeletion(){
        ArrayList<String> expectedAdditions = new ArrayList<>(
                Arrays.asList("\\draw (12,13) -- (54,12);"));
        assertEquals(expectedAdditions, commitModel.getRemovedLines());
    }

    @Test
    public void getCommitNumberReturnsRightNumber(){
        assertEquals("0x10003413", commitModel.getCommitNumber());
    }

    @Test
    public void getCommitEmptyContentReturnEmptyString() {
        //Added this test after encountering an issue, verify if a commit can be empty
        commitModel = new CommitModel("");
        assertEquals("", commitModel.getCommit());
    }
}