package be.ac.ulb.infof307.g06.models;

import be.ac.ulb.infof307.g06.models.importExport.ExportModel;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class TestExportModel {

    @Test
    void exportProject() {
        String dir = System.getProperty("user.dir") + File.separator + "test" + File.separator + "out" + File.separator + "exportProjectTest" + File.separator;
        String exportPath = dir + "exportProjectTest.tar";
        String filepath = dir + "exportProjectTest.tex";
        File fileToExport = new File(filepath);
        new File(dir).mkdirs();
        try {
            fileToExport.createNewFile();
            ExportModel.exportProject(exportPath, fileToExport);
        } catch (IOException e) {
            fail("Failed to the export project");
        }
        assertTrue(fileToExport.exists());
        fileToExport.delete();
        new File(exportPath).delete();
    }
}