package be.ac.ulb.infof307.g06.models;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestAccountModel {
    Database database;
    AccountModel accountModel;
    String username = "chanoine";
    String name = "Brandlock";
    String given_name = "Emile";
    String email = "noidea@ulb.be";
    String password ="verysecure";

    @BeforeAll
    void setup(){
        database = new Database();
        accountModel = new AccountModel();
    }

    @BeforeEach
    void eraseDatabase(){
        database.erase();
    }

    /**
     * To test if the signUp works correctly we test it by signing up a new
     * user and then we try to sign up a new user with a different username
     * and then again with a different mail but the same username as before.
     * This makes sure that all the given failure possibilities are tested.
     */
    @Test
    public void signUpReturnsMinusOneForFailure() {
        assertNotEquals(-1, accountModel.signUp(username, name, given_name, email, password));
        assertEquals(-1, accountModel.signUp("notSameUsername", name, given_name, email, password));
        assertEquals(-1, accountModel.signUp(username, name, given_name, "different@mail.com", password));
    }

    @Test
    public void signInUserReturnsExpectedId() {
        int id = accountModel.signUp(username, name, given_name, email, password);
        assertEquals(id, accountModel.signIn(username,password));
    }

    @Test
    public void wrongUsernameSignInReturnsMinusOne(){
        assertEquals(-1, accountModel.signIn("wrongUsername",password));
    }

    @Test
    public void wrongPasswordSignInReturnsMinusOne(){
        assertEquals(-1, accountModel.signIn(username,"wrongPassword"));
    }

    @Test
    public void wrongPasswordAndUsernameSignInReturnsMinusOne(){
        assertEquals(-1, accountModel.signIn("wrongUsername","wrongPassword"));
    }

    @Test
    public void goodUsernameUpdateReturnsNothing() {
        int id = accountModel.signUp(username, name, given_name, email, password);
        assertEquals("", accountModel.updateUsername(id, "camerlynck"));
    }

    @Test
    public void emptyUsernameUpdateReturnsEmptyErrorMessage() {
        int id = accountModel.signUp(username, name, given_name, email, password);
        String message = "Please enter a new username.";
        assertEquals(message , accountModel.updateUsername(id, ""));
    }

    @Test
    public void usernameNotAvailableUpdateReturnNotAvailableErrorMessage() {
        accountModel.signUp(username, name, given_name, email, password);
        String _username = "charles";
        String _email = "charles@degaule.com";
        int id = accountModel.signUp(_username, name, given_name, _email, password);
        String message = "Username not available.";
        assertEquals(message, accountModel.updateUsername(id, "chanoine"));
    }

    @Test
    public void badUsernameFormatUpdateReturnsInvalidUsernameMessage() {
        int id = accountModel.signUp(username, name, given_name, email, password);
        String message = "Please enter a valid username (without special characters and upper cases).";
        assertEquals(message , accountModel.updateUsername(id, "Camerlynck"));
    }



    @Test
    public void goodEmailUpdateReturnNothing() {
        int id = accountModel.signUp(username, name, given_name, email, password);
        assertEquals("", accountModel.updateEmail(id, "rubiks@cube.com"));
    }

    @Test
    public void emptyEmailUpdateReturnsEmptyErrorMessage() {
        int id = accountModel.signUp(username, name, given_name, email, password);
        String message = "Please enter a new email.";
        assertEquals(message , accountModel.updateEmail(id, ""));
    }

    @Test
    public void emailNotAvailableUpdateReturnsNotAvailableErrorMessage() {
        accountModel.signUp(username, name, given_name, email, password);
        String _username = "charles";
        String _email = "charles@degaule.com";
        int id = accountModel.signUp(_username, name, given_name, _email, password);
        String message = "Email not available.";
        assertEquals(message, accountModel.updateEmail(id, "noidea@ulb.be"));
    }

    @Test
    public void badEmailFormatUpdateReturnsInvalidEmailMessage() {
        int id = accountModel.signUp(username, name, given_name, email, password);
        String message = "The email is not valid.";
        assertEquals(message , accountModel.updateEmail(id, "carambarhotmail.be"));
    }




    @Test
    public void goodPasswordUpdateReturnNothing() {
        int id = accountModel.signUp(username, name, given_name, email, password);
        String newPasswordOne = "GoodPassword";
        String newPasswordTwo = "GoodPassword";
        assertEquals("", accountModel.updatePassword(id, password, newPasswordOne, newPasswordTwo));
    }

    @Test
    public void oldPasswordIncorrectUpdateReturnsOldPasswordIncorrectMessage() {
        int id = accountModel.signUp(username, name, given_name, email, password);
        String newPasswordOne = "GoodPassword";
        String newPasswordTwo = "GoodPassword";
        String message = "Your old password is incorrect.";
        assertEquals(message, accountModel.updatePassword(id, "CarottesAuChoux", newPasswordOne, newPasswordTwo));
    }

    @Test
    public void firstPasswordEmptyUpdateReturnsFirstPasswordEmptyErrorMessage() {
        int id = accountModel.signUp(username, name, given_name, email, password);
        String newPasswordOne = "";
        String newPasswordTwo = "GoodPassword";
        String message = "Please enter a new password.";
        assertEquals(message, accountModel.updatePassword(id, password, newPasswordOne, newPasswordTwo));
    }

    @Test
    public void secondPasswordEmptyUpdateReturnsSecondPasswordNotValidErrorMessage() {
        int id = accountModel.signUp(username, name, given_name, email, password);
        String newPasswordOne = "GoodPassword";
        String newPasswordTwo = "";
        String message = "Please confirm your new password.";
        assertEquals(message, accountModel.updatePassword(id, password, newPasswordOne, newPasswordTwo));
    }

    @Test
    public void newPasswordNotTheSameUpdateReturnsNotSamePasswordErrorMessage() {
        int id = accountModel.signUp(username, name, given_name, email, password);
        String newPasswordOne = "GoodPassword";
        String newPasswordTwo = "BadPassword";
        String message = "Please enter the same password to confirm.";
        assertEquals(message, accountModel.updatePassword(id, password, newPasswordOne, newPasswordTwo));
    }

    @Test
    public void newPasswordNotLongEnoughUpdateReturnsMinimumPasswordLengthErrorMessage() {
        int id = accountModel.signUp(username, name, given_name, email, password);
        String newPasswordOne = "short";
        String newPasswordTwo = "short";
        String message = "Please enter a password of minimum 8 characters.";
        assertEquals(message, accountModel.updatePassword(id, password, newPasswordOne, newPasswordTwo));
    }

}