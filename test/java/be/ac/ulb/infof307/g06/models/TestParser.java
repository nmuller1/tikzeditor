package be.ac.ulb.infof307.g06.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestParser {
    Parser parser = new Parser();

    @BeforeEach
    void setUp() {
        parser.setText("\\draw[color=red] (100, 100) rectangle (400, 400);\n" +
                "\\draw[color=blue] (100, 100) -- (273, 412) -- (318, 123);\n" +
                "\\fill[color=blue] (200, 200) circle (100pt);");
    }

    @Test
    void getNextInstruction() {
        String instruction = parser.getNextInstruction();
        assertEquals("\\draw[color=red] (100, 100) rectangle (400, 400)", instruction);
        instruction = parser.getNextInstruction();
        assertEquals("\\draw[color=blue] (100, 100) -- (273, 412) -- (318, 123)", instruction);
        instruction = parser.getNextInstruction();
        assertEquals("\\fill[color=blue] (200, 200) circle (100pt)", instruction);
    }

    @Test
    void getElementaryInstruction() {
        String instruction = parser.getNextInstruction();
        String elemInstr = parser.getElementaryInstruction(instruction);
        assertEquals("draw", elemInstr);
        instruction = parser.getNextInstruction();
        elemInstr = parser.getElementaryInstruction(instruction);
        assertEquals("draw", elemInstr);
        instruction = parser.getNextInstruction();
        elemInstr = parser.getElementaryInstruction(instruction);
        assertEquals("fill", elemInstr);
    }

    @Test
    void getInstructionOptions() {
        String instruction = parser.getNextInstruction();
        String[] instrOpt = parser.getInstructionOptions(instruction);
        assertEquals("red", instrOpt[0]);
        instruction = parser.getNextInstruction();
        instrOpt = parser.getInstructionOptions(instruction);
        assertEquals("blue", instrOpt[0]);
        instruction = parser.getNextInstruction();
        instrOpt = parser.getInstructionOptions(instruction);
        assertEquals("blue", instrOpt[0]);
    }

    @Test
    void getLinkInstruction() {
        String instruction = parser.getNextInstruction();
        String link = parser.getLinkInInstruction(instruction);
        assertEquals("rectangle", link);
        instruction = parser.getNextInstruction();
        link = parser.getLinkInInstruction(instruction);
        assertEquals("--", link);
        instruction = parser.getNextInstruction();
        link = parser.getLinkInInstruction(instruction);
        assertEquals("circle", link);
    }

    @Test
    void getCoordinates() {
        String instruction = parser.getNextInstruction();
        Double[][] coord = parser.getCoordinates(instruction);
        assertEquals(100, coord[0][0]);
        assertEquals(100, coord[0][1]);
        assertEquals(400, coord[1][0]);
        assertEquals(400, coord[1][1]);
        instruction = parser.getNextInstruction();
        coord = parser.getCoordinates(instruction);
        assertEquals(100, coord[0][0]);
        assertEquals(100, coord[0][1]);
        assertEquals(273, coord[1][0]);
        assertEquals(412, coord[1][1]);
        assertEquals(318, coord[2][0]);
        assertEquals(123, coord[2][1]);
        instruction = parser.getNextInstruction();
        coord = parser.getCoordinates(instruction);
        assertEquals(200, coord[0][0]);
        assertEquals(200, coord[0][1]);
        assertEquals(100, coord[1][0]);
    }
}
