package be.ac.ulb.infof307.g06.models;

import be.ac.ulb.infof307.g06.models.drawing.TriangleModel;
import javafx.scene.paint.Color;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestTriangleModel {
    private TriangleModel triangle;

    /**
     * Create a new triangle object for tests
     */
    @BeforeAll
    void setUp() {
        triangle = new TriangleModel("triangle", Color.RED, 10, new Double[]{10.0, 10.0});
    }

    /**
     * Test the x coordinates of all 3 vertices getter
     */
    @Test
    void testGetXVertices(){
        Double[] xVertices = triangle.getXVertices();
        Double[] currentXVertices = {10.0, 20.0, 15.0};
        assertEquals(currentXVertices[0], xVertices[0]);
        assertEquals(currentXVertices[1], xVertices[1]);
        assertEquals(currentXVertices[2], xVertices[2]);
    }

    /**
     * Test the y coordinates of all 3 vertices getter
     */
    @Test
    void testGetYVertices(){
        Double[] yVertices = triangle.getYVertices();
        Double[] currentYVertices = {10.0, 10.0, 15.0};
        assertEquals(currentYVertices[0], yVertices[0]);
        assertEquals(currentYVertices[1], yVertices[1]);
        assertEquals(currentYVertices[2], yVertices[2]);
    }
}