package be.ac.ulb.infof307.g06.models;

import be.ac.ulb.infof307.g06.models.importExport.FolderCompressionModel;
import be.ac.ulb.infof307.g06.models.importExport.ImportModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

class TestImportModel {
    private ImportModel importModel;
    private String outputPath;


    @BeforeEach
    public void setUp() {
        this.importModel = new ImportModel(new UserInfoModel(6, "test"));
        this.outputPath = System.getProperty("user.dir") + File.separator + "test" + File.separator + "out" + File.separator + "ImportProjectTest" + File.separator;
        new File(outputPath).mkdirs();
    }

    /**
     * Test the output path getter, where to save imported files
     */
    @Test
    void isPathCorrect() {
        File filePath = new File(this.outputPath + "sample.jpg");
        assertEquals(this.outputPath, ImportModel.getPathFromFile(filePath)+"/");
    }

    /**
     * Tests if the setters rejects an odd userID
     */
    @Test
    void setIfUserInfoChanged() {
        this.importModel.setUserInfoModel(new UserInfoModel(5, "test"));
        assertEquals(5, this.importModel.getUserInfoID());
    }

    /**
     * Tests if the setters rejects an odd userID
     */
    @Test
    void setIfUserInfoUnchanged() {
        this.importModel.setUserInfoModel(new UserInfoModel(-5, "test"));
        assertEquals(6, this.importModel.getUserInfoID());
    }

    /**
     * Creates an odd sample .tex file
     */
    private String createSampleFile() {
        String tarFile = this.outputPath+"importTest.tar";
        try {
            File file = new File(outputPath + "test.tex");
            FileWriter writer;
            writer = new FileWriter(file);
            writer.write("\\draw (100, 100) circle (100pt);");
            writer.close();
            new FolderCompressionModel().compress(tarFile, file);
        } catch (IOException e) {
            fail("Unable to create sample file");
        }
        return tarFile;
    }

    /**
     * Tests if the imported file exists
     * try/catch are here to prevent test from crashing
     */
    @Test
    void importProject() {
        try {
            File file = new File(createSampleFile());
            this.importModel.importProject(file, outputPath);
            assertTrue(new File(this.outputPath+"test.tex").exists());
            file.delete();
            new File(outputPath + "test.tex").delete();
        } catch (Exception e) {
            fail("Test importProject: unable to import project");
        }
    }
}