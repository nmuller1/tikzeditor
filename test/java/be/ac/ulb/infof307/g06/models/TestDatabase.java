package be.ac.ulb.infof307.g06.models;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestDatabase {
    Database database;
    String testFileName = "testFile";
    String testFileName2 = "testFile2";
    String newTestFileName = "newTestFile";
    String pathToFile = "/test/testFile";
    String pathToFile2 = "/test/testFile2";
    String newPathToFile = "/test/testFile";
    String testMessage = "message de commit";
    String testBranchName = "testBranch";
    String testBranchName2 = "testBranch2";
    String username = "test";
    String name = "testName";
    String givenName = "testGivenName";
    String email = "testEmail";
    String password = "testPassword";
    String newEmail = "TristanEmail";
    String newPassword = "TristanEmail";
    String newUsername = "TristanUsername";

    UserInfoModel testUserInfoModel;
    @BeforeAll
    void setUp() {
        database = new Database();
        database.erase();
        Integer userID =  database.createUser("testUsername", "TestLastName", "TestFirstName", "test@test.be", "test1234");
        testUserInfoModel = new UserInfoModel(userID, "Test");
    }

    @AfterAll
    void clean() {
        database.erase();
    }

    @Test
    void insertProjectAndGetProjectData() {
        int projectID = database.insertProject(testUserInfoModel.getUserID(), testFileName, pathToFile);
        String[] data = database.getProjectData(projectID);
        assertEquals(testFileName, data[0]);
        assertEquals(pathToFile, data[1]);
        database.deleteProject(projectID);
    }

    @Test
    void getProjectsOfUser() {
        int projectID = database.insertProject(testUserInfoModel.getUserID(), testFileName, pathToFile);
        int projectID2 = database.insertProject(testUserInfoModel.getUserID(), testFileName2, pathToFile2);
        List<Integer> listOfProjectID = database.getProjectsOfUser(testUserInfoModel.getUserID());
        assertEquals(listOfProjectID.get(0), projectID);
        assertEquals(listOfProjectID.get(1), projectID2);
        database.deleteProject(projectID);
        database.deleteProject(projectID2);
    }

    @Test
    void renameProject() {
        int projectID = database.insertProject(testUserInfoModel.getUserID(), testFileName, pathToFile);
        database.renameProject(projectID, newTestFileName);
        String[] data = database.getProjectData(projectID);
        assertEquals(newTestFileName, data[0]);
        assertEquals(pathToFile, data[1]);
        database.deleteProject(projectID);
    }

    @Test
    void updateModificationDate() {

    }

    @Test
    void updatePath() {
        int projectID = database.insertProject(testUserInfoModel.getUserID(), testFileName, pathToFile);
        database.updatePath(projectID, newPathToFile);
        String[] data;
        data = database.getProjectData(projectID);
        assertEquals(testFileName, data[0]);
        assertEquals(newPathToFile, data[1]);
        database.deleteProject(projectID);
    }

    @Test
    void deleteProject() {
        int projectID = database.insertProject(testUserInfoModel.getUserID(), testFileName, pathToFile);
        int branchID = database.insertBranch(projectID,testBranchName);
        int commitID = database.insertCommit(projectID,branchID,testMessage);
        database.deleteProject(projectID);
        String[] data = database.getProjectData(projectID);
        String[] branchData = database.getBranchData(projectID,branchID);
        String[] commitData = database.getCommitData(projectID,branchID,commitID);
        assertNull(data[0]);
        assertNull(branchData[0]);
        assertNull(commitData[0]);
    }

    @Test
    void checkProjectExist() {
        int projectID = database.insertProject(testUserInfoModel.getUserID(), testFileName, pathToFile);
        assertTrue(database.checkProjectExist(pathToFile));
        assertFalse(database.checkProjectExist("salut"));
        database.deleteProject(projectID);
    }

    @Test
    void insertBranchAndGetBranchData() {
        database.erase();
        int projectID = database.insertProject(testUserInfoModel.getUserID(), testFileName, pathToFile);
        int branchID = database.insertBranch(projectID,testBranchName);
        String[] data = database.getBranchData(projectID,branchID);
        assertEquals(Integer.toString(projectID), data[0]);
        assertEquals(testBranchName, data[2]);
        assertEquals("1", data[4]);
        database.deleteProject(projectID);
    }

    @Test
    void getLastBranchID() {
        database.erase();
        int projectID = database.insertProject(testUserInfoModel.getUserID(), testFileName, pathToFile);
        int branchID = database.insertBranch(projectID,testBranchName);
        int lastBranchID = database.getLastBranchID(projectID);
        assertEquals(branchID,lastBranchID);
        database.deleteProject(projectID);
    }

    @Test
    void updateBranchStatus() {
        database.erase();
        int projectID = database.insertProject(testUserInfoModel.getUserID(), testFileName, pathToFile);
        int branchID = database.insertBranch(projectID,testBranchName);
        database.updateBranchStatus(projectID,branchID,0);
        String[] data = database.getBranchData(projectID,branchID);
        assertEquals("0", data[4]);
        database.deleteProject(projectID);
    }

    @Test
    void getBranches() {
        database.erase();
        int projectID = database.insertProject(testUserInfoModel.getUserID(), testFileName, pathToFile);
        int branch1ID = database.insertBranch(projectID,testBranchName);
        int branch2ID = database.insertBranch(projectID,testBranchName2);
        List<Integer> listOfBranchID = database.getBranches(projectID);
        assertEquals(listOfBranchID.get(0),branch1ID);
        assertEquals(listOfBranchID.get(1),branch2ID);
        database.deleteProject(projectID);
    }

    @Test
    void getActiveBranches() {
        database.erase();
        int projectID = database.insertProject(testUserInfoModel.getUserID(), testFileName, pathToFile);
        int branch1ID = database.insertBranch(projectID,testBranchName);
        int branch2ID = database.insertBranch(projectID,testBranchName2);
        database.updateBranchStatus(projectID,branch1ID,0);
        List<Integer> listOfActiveBranchID = database.getActiveBranches(projectID);
        assertEquals(listOfActiveBranchID.size(), 1);
        assertEquals(listOfActiveBranchID.get(0),branch2ID);
        database.deleteProject(projectID);
    }

    @Test
    void insertCommitAndGetCommitData() {
        database.erase();
        int projectID = database.insertProject(testUserInfoModel.getUserID(), testFileName, pathToFile);
        int branchID = database.insertBranch(projectID,testBranchName);
        int commitID = database.insertCommit(projectID,branchID,testMessage);
        String[] data = database.getCommitData(projectID,branchID,commitID);
        assertEquals(Integer.toString(projectID), data[0]);
        assertEquals(Integer.toString(branchID), data[1]);
        assertEquals(testMessage, data[3]);
        database.deleteProject(projectID);
    }

    @Test
    void getLastCommitID() {
        database.erase();
        int projectID = database.insertProject(testUserInfoModel.getUserID(), testFileName, pathToFile);
        int branchID = database.insertBranch(projectID,testBranchName);
        int commitID = database.insertCommit(projectID,branchID,testMessage);
        int lastCommitID = database.getLastCommitID(projectID,branchID);
        assertEquals(commitID,lastCommitID);
        database.deleteProject(projectID);
    }

    @Test
    void getCommits() {
        database.erase();
        int projectID = database.insertProject(testUserInfoModel.getUserID(), testFileName, pathToFile);
        int branchID = database.insertBranch(projectID,testBranchName);
        int commit1ID = database.insertCommit(projectID,branchID,testMessage);
        int commit2ID = database.insertCommit(projectID,branchID,testMessage);
        List<Integer> listOfCommitID = database.getCommits(projectID,branchID);
        assertEquals(listOfCommitID.get(0),commit1ID);
        assertEquals(listOfCommitID.get(1),commit2ID);
        database.deleteProject(projectID);
    }

    @Test
    void createUser() {
        database.erase();
        int userID = database.createUser(username, name, givenName, email, password);
        String[] userInfo = database.getInfoUser(userID);
        assertEquals(username, userInfo[0]);
        assertEquals(name, userInfo[1]);
        assertEquals(givenName, userInfo[2]);
        assertEquals(email, userInfo[3]);
        assertEquals(password, userInfo[4]);
    }

    @Test
    void getUserID() {
        database.erase();
        int userID = database.createUser(username, name, givenName, email, password);
        int testUserID = database.getUserID(username);
        assertEquals(userID, testUserID);
        int testInexistentID = database.getUserID("Antoine");
        assertEquals(-1, testInexistentID);
    }


    @Test
    void isUsernameAvailable() {
        database.erase();
        database.createUser(username, name, givenName, email, password);
        assertTrue(database.isUsernameAvailable("Antoine"));
        assertFalse(database.isUsernameAvailable("test"));
    }

    @Test
    void isEmailAvailable() {
        database.erase();
        database.createUser(username, name, givenName, email, password);
        boolean isAvailable = database.isEmailAvailable("AntoineEmail");
        boolean isNotAvailable = database.isEmailAvailable("testEmail");
        assertTrue(isAvailable);
        assertFalse(isNotAvailable);
    }

    @Test
    void modifyEmail() {
        int userID = database.createUser(username, name, givenName, email, password);
        database.modifyEmail(userID, newEmail);
        String[] userInfo = database.getInfoUser(userID);
        assertEquals(newEmail, userInfo[3]);
        assertNotEquals(email, userInfo[3]);
    }

    @Test
    void modifyPassword() {
        int userID = database.createUser(username, name, givenName, email, password);
        database.modifyPassword(userID, newPassword);
        String[] userInfo = database.getInfoUser(userID);
        assertEquals(newPassword, userInfo[4]);
        assertNotEquals(password, userInfo[4]);
    }

    @Test
    void modifyUsername() {
        database.erase();
        int userID = database.createUser(username, name, givenName, email, password);
        database.modifyUsername(userID, newUsername);
        String[] userInfo = database.getInfoUser(userID);
        assertEquals(newUsername, userInfo[0]);
        assertNotEquals(username, userInfo[0]);

    }
}