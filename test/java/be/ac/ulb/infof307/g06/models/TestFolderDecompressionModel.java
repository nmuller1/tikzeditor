package be.ac.ulb.infof307.g06.models;

import be.ac.ulb.infof307.g06.models.importExport.FolderCompressionModel;
import be.ac.ulb.infof307.g06.models.importExport.FolderDecompressionModel;
import org.apache.commons.compress.utils.Charsets;
import org.apache.commons.compress.utils.IOUtils;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TestFolderDecompressionModel {
    private Database database;
    private UserInfoModel userInfoModel;
    private String pathCompressed;
    private String pathDecompressed;
    private File file;
    private File tarFile;

    /**
     * Here, we set up the database and the test's user
     */
    @BeforeAll
    void setUp() {
        database = new Database();
        database.erase();
        userInfoModel = new UserInfoModel(
                database.createUser("usernametest", "Testlastname", "Testfirstname", "test@test.be", "Test1234"),
                "usernametest");
    }

    /**
     * Of course, we cancel everything at the end
     */
    @AfterAll
    void clean() {
        database.erase();
    }

    /**
     * Creates the test's compress and  test's decompress directories
     */
    private void createDirectories() {
        pathCompressed = System.getProperty("user.dir") + File.separator + "test" + File.separator + "out" + File.separator + "compressedTest" + File.separator;
        new File(pathCompressed).mkdirs();
        pathDecompressed = System.getProperty("user.dir") + File.separator + "test" + File.separator + "out" + File.separator +"decompressedTest" + File.separator;
        new File(pathDecompressed).mkdirs();
    }
    /**
     * This is only used in createFileCompressItThenDecompressIt()
     * Creates a sample .tex file
     */
    private void createFile() {
        file = new File(pathCompressed + "test.tex");
        try {
            FileWriter writer = new FileWriter(file);
            writer.write("\\draw (100, 100) circle (100pt);");
            writer.close();
        } catch (Exception e) {
            fail("File creation failed. Directory was: " + pathCompressed + " and error was: " + e.getMessage());
        }
    }

    /**
     * This is only used in createFileCompressItThenDecompressIt()
     * It compresses the .tex file
     */
    private void compressFile() {
        tarFile = new File(pathCompressed + File.separator + "test.tar");
        if (!tarFile.exists()) {
            try {
                FolderCompressionModel.compress(pathCompressed + File.separator + "test.tar", file);
            } catch (IOException e){
                fail("Compression failed");
            }
        }
    }

    /**
     * This is only used in createFileCompressItThenDecompressIt()
     * It compresses the .tar file
     */
    private void decompressFile() {
        (new FolderDecompressionModel(userInfoModel)).decompress(pathCompressed + "test.tar", pathDecompressed);
    }

    /**
     * Before each test, we compress the archive, and then we decompress it (in another folder)
     */
    @BeforeEach
    void createFileCompressItThenDecompressIt() {
        createDirectories();
        createFile();
        compressFile();
        decompressFile();
    }

    /**
     * And, of course, we cancel everything at the end
     */
    @AfterEach
    void deleteArchivesAndFiles() {
        new File(pathDecompressed + "test.tex").delete();
        new File(pathCompressed + "test.tex").delete();
        tarFile.delete();
    }

    /**
     * Checks if the file exists
     */
    @Test
    public void checkIfFileExists() {
        assertTrue(new File(pathDecompressed + "test.tex").exists());
    }
    /**
     * Check if the decompressed file has the right content
     */
    @Test
    public void checkIfContentIsCorrect() {
        try {
            String stringTest = new String(Files.readAllBytes(Paths.get(pathDecompressed + "test.tex")));
            assertEquals("\\draw (100, 100) circle (100pt);", stringTest);
        } catch (IOException e) {
            fail("Unable to read the extracted sample file");
        }
    }
}
