package be.ac.ulb.infof307.g06.models;

import be.ac.ulb.infof307.g06.utils.FileUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class that make the unitary tests.
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestManageProjectModel {
    ManageProjectModel model;
    Database database;
    String testFileContent;
    UserInfoModel userInfoModel;

    @BeforeAll
    void setup(){
        database = new Database();
        database.erase();
        Integer userID =  database.createUser("Test", "test", "test", "test", "test");
        testFileContent = "The file is not empty.";
        userInfoModel = new UserInfoModel(userID, "Test");
        model = new ManageProjectModel(userInfoModel);
    }

    /**
     * Method that test the updateFileInfo method by adding manually  a fileInfo in a list of the current fileInfo
     * of the user Test. Then compare the previous list to à list that is made out of the database.
     */
    @Test
    void updateUserFileInfo() {
        File testFile = new File("test/testFile");
        try {
            //Create a file to test the method.
            testFile.createNewFile();
            //Get the current user fileInfo list of the user.
            model.updateUserFileInfo();
            List<FileInfo> filesOfUserTest = model.getUserFileInfo();
            //Insert the test file in the database.
            int idTestFile = database.insertProject(userInfoModel.getUserID(), testFile.getName(), testFile.getAbsolutePath());
            FileInfo testFileInfo = new FileInfo( "testFile", "Test", "", testFile.getAbsolutePath(), idTestFile);
            //Add the file to the user fileInfo list created above.
            filesOfUserTest.add(testFileInfo);
            //Call the method to get the new fileInfo list of the current state of the database.
            model.updateUserFileInfo();
            List<FileInfo> filesOfUser = model.getUserFileInfo();
            //Compare if the file paths are the same of each file in the lists of fileInfo.
            boolean isEqual = true;
            if(filesOfUserTest.size() == filesOfUser.size()){
                for (int i = 0; i < filesOfUserTest.size(); i++) {
                    if(!(filesOfUserTest.get(i).getFilePath().equals(filesOfUser.get(i).getFilePath()))){
                        isEqual = false;
                    }
                }
            }
            else isEqual = false;
            //Delete files of Test user.
            List<Integer> idFilesOfUser = database.getProjectsOfUser(userInfoModel.getUserID());
            for (int i = 0; i < idFilesOfUser.size(); i++) {
                database.deleteProject(idFilesOfUser.get(i));
            }
            assertTrue(isEqual, "UpdateUserFileInfo method didn't executed properly");

        }  catch (IOException e) {
            fail("Couldn't update user files due to file manipulation exception!");
            e.printStackTrace();
        } finally {
            testFile.delete();
        }
    }

    @Test
    void copyProject(){
        File testFile = new File("test/testFile");
        try {
            testFile.createNewFile();
            FileWriter fileWriter = new FileWriter(testFile, false);
            fileWriter.write(testFileContent);
            fileWriter.close();
            FileInfo testFileInfo = new FileInfo( "testFile", "Test", "", testFile.getAbsolutePath(), 0);

            model.copyProject(testFileInfo);
            File testFileCopy = new File("test/testFile_copy");
            if(testFileCopy.exists()){
                String content = FileUtils.fileToString(testFileCopy.getAbsolutePath());
                assertEquals(testFileContent, content, "copyProject method didn't executed properly");
            }
            else{
                fail("Couldn't copy file!");
            }
            //Delete files of Test user.
            List<Integer> idFilesOfUser = database.getProjectsOfUser(userInfoModel.getUserID());
            for (int i = 0; i < idFilesOfUser.size(); i++) {
                database.deleteProject(idFilesOfUser.get(i));
            }
            testFileCopy.delete();
        } catch (IOException e) {
            fail("Couldn't copy file due to file manipulation exception!");
            e.printStackTrace();
        } finally {
            testFile.delete();
        }
    }


    @Test
    void deleteProject() {
        File testFile = new File("test/testFile");
        try{
            testFile.createNewFile();
            int idTestFile = database.insertProject(userInfoModel.getUserID(), testFile.getName(), testFile.getAbsolutePath());
            FileInfo testFileInfo = new FileInfo( "testFile", "Test", "", testFile.getAbsolutePath(), idTestFile);
            model.deleteProject(testFileInfo);
            assertTrue(!(testFile.exists() && database.checkProjectExist(testFile.getAbsolutePath())),"deleteProject method didn't executed properly");

        } catch (IOException e) {
            testFile.delete();
            fail("Couldn't delete file due to file manipulation exception!");
            e.printStackTrace();
        }
    }

    @Test
    void changeDirectory(){
        File changeDirectory = new File("test/changeTest");
        File testFile = new File("test/testFile");
        try {
            changeDirectory.mkdir();
            testFile.createNewFile();
            FileWriter fileWriter = new FileWriter(testFile, false);
            fileWriter.write(testFileContent);
            fileWriter.close();
            int projectId = database.insertProject(userInfoModel.getUserID(),"testFile",testFile.getAbsolutePath());
            FileInfo testFileInfo = new FileInfo( "testFile", userInfoModel.getUsername(), "", testFile.getAbsolutePath(), projectId);
            model.changeDirectory(testFileInfo,"test/changeTest");
            File testFileMove = new File("test/changeTest/testFile");

            if(testFileMove.exists()){
                String content = FileUtils.fileToString(testFileMove.getAbsolutePath());
                assertEquals(testFileContent, content, "");
                assertEquals("test/changeTest/testFile",database.getProjectData(projectId)[1]);
            }
            else{
                fail("Couldn't move file!");
            }
            testFileMove.delete();
        } catch (IOException e) {
            fail("Couldn't move file due to file manipulation exception!");
            e.printStackTrace();
        } finally {
            testFile.delete();
            changeDirectory.delete();
        }
    }

    @Test
    void renameProject() {
        database.erase();
        File folders = new File(System.getProperty("user.dir")+"/test/out/manageProjectTest/");
        folders.mkdirs();
        File file = new File(System.getProperty("user.dir")+"/test/out/manageProjectTest/testFile");
        try {
            FileWriter writer;
            writer = new FileWriter(file);
            writer.write("\\draw (100, 100) circle (100pt);");
            writer.close();
        } catch (Exception e) {
            fail("Unable to create the sample file");
        }
        int id = database.insertProject(123,"testFile", file.getAbsolutePath());
        FileInfo fileInfo = new FileInfo("testFile", "user","timestamp",file.getAbsolutePath(),id);

        String oldPath = fileInfo.getFilePath();
        String oldName = oldPath.substring(oldPath.lastIndexOf(File.separator));
        String newName = "newName";
        String newPath = System.getProperty("user.dir")+"/test/out/manageProjectTest/";
        model.renameProject(newName, fileInfo);

        String[] data = database.getProjectData(id);

        assertEquals(newPath+newName,data[1]);
        assertNotEquals(oldPath,data[1]);
        assertEquals(newName,data[0]);
        assertNotEquals(oldName,data[0]);
        new File(newPath+newName).delete();
    }

    @Test
    void editProject() {
        File testFile = new File("test/out/manageProjectTest/testFile");
        try {
            testFile.createNewFile();
            FileWriter fileWriter = new FileWriter(testFile, false);
            fileWriter.write(testFileContent);
            fileWriter.close();

            FileInfo testFileInfo = new FileInfo( "testFile", "Test", "", testFile.getAbsolutePath(), 0);
            model.editProject(testFileInfo);
            String content = FileUtils.fileToString(testFile.getAbsolutePath());
            assertEquals(testFileContent, content, "editProject method didn't executed properly");
        } catch (IOException e) {
            fail("Couldn't edit project due to file manipulation exception!");
            e.printStackTrace();
        } finally {
            testFile.delete();
        }
    }

    @Test
    void copyNameFile() {
        assertEquals("slipdebain_copy.tex", model.copyNameFile("slipdebain.tex"));
        assertEquals("slipdebain_copy", model.copyNameFile("slipdebain"));
        assertNotEquals("slipdebain.tex_copy", model.copyNameFile("slipdebain.tex")); // encountered error during development
    }
}
