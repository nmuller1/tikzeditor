package be.ac.ulb.infof307.g06.models;

import be.ac.ulb.infof307.g06.models.drawing.ClickStack;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test instance for the ClickStack class which is used to save two clicks
 * of an user and then pop them.
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestClickStack {

    private ClickStack clickStack;

    @BeforeAll
    void setUp() {
        clickStack = new ClickStack();
    }

    /**
     * Simulate two user inputs with coordinates (20.0, 2.0) and (47.0, 90.0) respectively
     */
    @Test
    void pushTwoClicksOnStackAndSeeIfTheyPopInCorrectOrder() {
        Double[] testClick1 = {20.0, 2.0};
        Double[] testClick2 = {47.0, 90.0};
        clickStack.push(testClick1);
        clickStack.push(testClick2);
        assertEquals(testClick2, clickStack.pop());
        assertEquals(testClick1, clickStack.pop());
    }
}