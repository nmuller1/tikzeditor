package be.ac.ulb.infof307.g06.views.edituser;

import be.ac.ulb.infof307.g06.controllers.listeners.ModifyUserListener;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * General form of the different pages modifying the user infos.
 */
public class ModifyUserView {
    private ModifyUserListener listener;
    private Scene scene;
    protected VBox generalVBox;
    protected HBox buttonsHBox;

    //Getters and setters
    public Scene getScene() {return scene;}
    public void setScene(Scene scene) { this.scene = scene; }
    public void setListener(ModifyUserListener listener) { this.listener = listener; }

    /**
     * Create the confirm and cancel buttons.
     */
    protected void initButtons() {
        buttonsHBox = new HBox();
        buttonsHBox.setAlignment(Pos.CENTER);
        buttonsHBox.setPrefWidth(200);

        Button confirmButton = new Button("Confirm");
        confirmButton.setMinWidth(generalVBox.getPrefWidth());
        confirmButton.setOnAction(this::confirm);

        Button cancelButton = new Button("Cancel");
        cancelButton.setMinWidth(generalVBox.getPrefWidth());
        cancelButton.setOnAction(this::cancel);

        buttonsHBox.getChildren().addAll(confirmButton, cancelButton);
    }

    private void confirm(ActionEvent event) { listener.confirmButtonClicked(); }
    private void cancel(ActionEvent event) { listener.cancelButtonClicked(); }
}
