package be.ac.ulb.infof307.g06.views;

import be.ac.ulb.infof307.g06.controllers.listeners.SignUpListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

import java.io.IOException;

public class SignUpView extends HBox {
    private final Scene scene;
    @FXML
    private TextField usernameTextField;

    @FXML
    private TextField firstNameTextField;

    @FXML
    private TextField lastNameTextField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private PasswordField confirmPasswordField;

    @FXML
    private CheckBox eulaCheckBox;

    @FXML
    private Hyperlink eulaHyperlink;

    @FXML
    private Button confirmButton;

    @FXML
    private Hyperlink signInHyperlink;

    private SignUpListener listener;
    @FXML
    private TextField emailTextField;

    public SignUpView() {
        loadFxml();
        scene = new Scene(this);
    }

    @FXML
    public void confirmButtonClicked(ActionEvent event) {
        String username = usernameTextField.getText();
        String firstName = firstNameTextField.getText();
        String lastName = lastNameTextField.getText();
        String password = passwordField.getText();
        String confirmPassword = confirmPasswordField.getText();
        String email = emailTextField.getText();
        boolean agreed = eulaCheckBox.isSelected();
        listener.confirmButtonClicked(username, email, firstName, lastName, password, confirmPassword, agreed);
    }

    @FXML
    public void eulaHyperlinkClicked(ActionEvent event) {
        listener.eulaHyperlinkClicked();
    }

    @FXML
    public void signInHyperlinkClicked(ActionEvent event) {
        listener.signInHyperlinkClicked();
    }

    private void loadFxml() {
        FXMLLoader fxmlLoader = new FXMLLoader(SignUpView.class.getResource("/fxml/SignUpView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setControllerFactory(SignUpView -> this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void setListener(SignUpListener listener) {
        this.listener = listener;
    }
}
