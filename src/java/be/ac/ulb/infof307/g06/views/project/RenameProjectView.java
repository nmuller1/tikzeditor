package be.ac.ulb.infof307.g06.views.project;

import be.ac.ulb.infof307.g06.controllers.listeners.RenameProjectListener;
import be.ac.ulb.infof307.g06.models.FileInfo;
import be.ac.ulb.infof307.g06.views.FxmlUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 * Class that displays a window to rename a given file.
 */
public class RenameProjectView extends AnchorPane {
    @FXML
    private TextField textField;
    private FileInfo fileInfo;
    private RenameProjectListener listener;
    private final Scene scene;

    public RenameProjectView() {
        FxmlUtils.loadFxml(this);
        scene = new Scene(this);
    }

    /**
     * Action on okButton clicked
     * @param event
     */
    @FXML
    private void okButtonClicked(ActionEvent event) {
        listener.renameFile(textField.getText(), fileInfo);
        listener.closeWindow();
    }

    /**
     * action on cancelButton clicked
     * @param event
     */
    @FXML
    private void cancelButtonClicked(ActionEvent event) {
        listener.closeWindow();
    }

    /**
     * Setter to set the controller associated to RenameProjectView.
     * @param listener RenameProjectController the controller associated to RenameProjectView.
     */
    public void setListener(RenameProjectListener listener) {
        this.listener = listener;
    }

    public void setFileInfo(FileInfo fileInfo) {
        this.fileInfo = fileInfo;
        textField.setText(fileInfo.getProjectName());
    }
}
