package be.ac.ulb.infof307.g06.views.edituser;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

/**
 * Create a page for modifying the password of the user.
 */
public class ModifyPasswordView extends ModifyUserView {
    private PasswordField oldPasswordField;
    private PasswordField newPasswordFieldOne;
    private PasswordField newPasswordFieldTwo;

    public ModifyPasswordView() {
        initLayout();
    }

    //Getters and setters.
    public String getOldPasswordFieldText() { return oldPasswordField.getText(); }
    public String getNewPasswordFieldOneText() { return newPasswordFieldOne.getText(); }
    public String getNewPasswordFieldTwoText() { return newPasswordFieldTwo.getText(); }

    /**
     * Initialize the general layout for modifying the username.
     */
    private void initLayout() {
        generalVBox = new VBox(20);
        generalVBox.setAlignment(Pos.CENTER);
        generalVBox.setPrefWidth(200);
        initPasswordFields();
        initButtons();
        generalVBox.getChildren().addAll(oldPasswordField, newPasswordFieldOne, newPasswordFieldTwo, buttonsHBox);
        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(generalVBox);
        setScene(new Scene(borderPane, 600,600));
    }

    /**
     * Initialize all the password fields needed.
     * The old one that will be confirmed and the new one that we verify twice.
     */
    private void initPasswordFields() {
        oldPasswordField = new PasswordField();
        oldPasswordField.setPromptText("Please enter your old password");
        newPasswordFieldOne = new PasswordField();
        newPasswordFieldOne.setPromptText("New password");
        newPasswordFieldTwo = new PasswordField();
        newPasswordFieldTwo.setPromptText("Please re-type your new password");
    }
}
