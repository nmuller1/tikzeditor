package be.ac.ulb.infof307.g06.views.drawing;

import be.ac.ulb.infof307.g06.controllers.listeners.ToolBarListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Spinner;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.paint.Color;

import java.io.IOException;

/**
 * this is the toolbar for the tool bar where the user can choose what he wants to do
 */
public class ToolBarView extends ToolBar {
    private final Scene scene;
    @FXML
    private ToggleGroup shapeToggleGroup;
    @FXML
    private ColorPicker colorPicker;
    @FXML
    private Spinner<Integer> sizeSpinner;
    @FXML
    private Button translateButton;
    @FXML
    private Button clearButton;
    @FXML
    private Button previewButton;

    private ToolBarListener listener;

    public ToolBarView() {
        loadFxml();
        scene = new Scene(this);
    }

    /**
     * loads fxml to diplay the view
     */
    private void loadFxml() {
        FXMLLoader fxmlLoader = new FXMLLoader(ToolBarView.class.getResource("/fxml/ToolBarView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setControllerFactory(ToolBarView -> this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    /**
     * when clear button is clicked
     */
    @FXML
    void clearButtonClicked(ActionEvent event) {
        listener.clearButtonClicked();
    }

    @FXML
    /**
     * when translate button is clicked
     */
    void translateButtonClicked(ActionEvent event) {
        listener.translateButtonClicked();
    }

    /**
     * when preview button is clicked
     */
    @FXML
    void previewButtonClicked(ActionEvent event) {
        listener.previewButtonClicked();
    }

    /**
     * Getter to get the shape size selected.
     * @return int the size of the shape required.
     */
    public int getShapeSize() {
        return sizeSpinner.getValue();
    }

    /**
     * Getter to get the shape type.
     * @return string the name of the button name selected.
     */
    public String getShapeType() {
        return shapeToggleGroup.getSelectedToggle().getUserData().toString();
    }

    /**
     * Getter that gives the color wanted.
     * @return color the color selected.
     */
    public Color getShapeColor() {
        return colorPicker.getValue();
    }

    public void setListener(ToolBarListener listener) {
        this.listener = listener;
    }

}
