package be.ac.ulb.infof307.g06.views.help;

import be.ac.ulb.infof307.g06.controllers.HelpController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;

import java.io.IOException;

public class HelpView extends BorderPane {
    private final HelpController helpController;
    private final Scene scene;
    private final TreeItem rootItem;
    @FXML
    private TreeView<?> sideBarTreeView;
    @FXML
    private Text helpContent;

    public HelpView(HelpController helpController) {
        scene = new Scene(this);
        this.helpController = helpController;
        loadFxml();
        rootItem = new TreeItem("Root node");
        TreeItem gettingStartedItem = new TreeItem("Pour commencer");

        TreeItem overViewItem = new TreeItem("Aperçu de l'interface");
        overViewItem.getChildren().add(new TreeItem("Création d'un projet"));
        overViewItem.getChildren().add(new TreeItem("Ouverture d'un projet existant"));

        TreeItem editUserItem = new TreeItem("Modification des données utilisateur");
        editUserItem.getChildren().add(new TreeItem("Modification du nom d'utilisateur"));
        editUserItem.getChildren().add(new TreeItem("Modification de l'adresse mail"));
        editUserItem.getChildren().add(new TreeItem("Modification du mot de passe"));
        overViewItem.getChildren().add(editUserItem);

        TreeItem editionItem = new TreeItem("L'édition de projet");
        editionItem.getChildren().add(new TreeItem("La console d'édition"));
        editionItem.getChildren().add(new TreeItem("Le canvas d'édition"));
        editionItem.getChildren().add(new TreeItem("La barre d'outils"));
        gettingStartedItem.getChildren().add(overViewItem);
        rootItem.getChildren().add(gettingStartedItem);
        rootItem.getChildren().add(editionItem);

        TreeItem tikZItem = new TreeItem("Le language TikZ");
        tikZItem.getChildren().add(new TreeItem("Syntaxe primaire"));
        tikZItem.getChildren().add(new TreeItem("Dessiner des formes"));
        tikZItem.getChildren().add(new TreeItem("Dessiner des liens"));
        rootItem.getChildren().add(tikZItem);

        sideBarTreeView.setRoot(rootItem);
        sideBarTreeView.setShowRoot(false);

        helpContent.wrappingWidthProperty().bind(scene.widthProperty());
        sideBarTreeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> setHelpContent());
        helpContent.setText(this.helpController.getHelpFileContent("/help/gettingStartedHelp.html"));
    }

    public void setHelpContent() {
        String filePath;
        switch (sideBarTreeView.getSelectionModel().getSelectedItems().get(0).getValue().toString()) {
            case ("Aperçu de l'interface"):
                filePath = "interfaceOverviewHelp.html";
                break;
            case ("Création d'un projet"):
                filePath = "createProjectHelp.html";
                break;
            case ("Ouverture d'un projet existant"):
                filePath = "openProjectHelp.html";
                break;
            case ("Modification des données utilisateur"):
                filePath = "editUserProfileHelp.html";
                break;
            case ("Le language TikZ"):
                filePath = "TikZHelp.html";
                break;
            case ("Modification du nom d'utilisateur"):
                filePath = "changeUsernameHelp.html";
                break;
            case ("Modification de l'adresse mail"):
                filePath = "changeMailHelp.html";
                break;
            case ("Modification du mot de passe"):
                filePath = "changePasswordHelp.html";
                break;
            case ("L'édition de projet"):
                filePath = "editingHelp.html";
                break;
            case ("La console d'édition"):
                filePath = "consoleHelp.html";
                break;
            case ("Le canvas d'édition"):
                filePath = "drawingCanvasHelp.html";
                break;
            case ("La barre d'outils"):
                filePath = "toolBarHelp.html";
                break;
            case ("Syntaxe primaire"):
                filePath = "tikZPrimarySyntaxHelp.html";
                break;
            case ("Dessiner des formes"):
                filePath = "tikZShapeHelp.html";
                break;
            case ("Dessiner des liens"):
                filePath = "tikZBindHelp.html";
                break;
            default:
                filePath = "gettingStartedHelp.html";
                break;
        }
        helpContent.setText(helpController.getHelpFileContent("/help/" + filePath));
    }

    private void loadFxml() {
        FXMLLoader fxmlLoader = new FXMLLoader(HelpView.class.getResource("/fxml/HelpView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setControllerFactory(HelpView -> this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }
}
