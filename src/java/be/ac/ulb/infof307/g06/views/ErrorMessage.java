package be.ac.ulb.infof307.g06.views;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ErrorMessage {
    private final Stage stage;

    public ErrorMessage(Stage primaryStage, String errorString) {
        stage = new Stage();
        stage.initOwner(primaryStage);
        stage.initModality(Modality.WINDOW_MODAL);
        Text text = new Text();
        text.setText(errorString);
        text.setWrappingWidth(250);
        text.setTextAlignment(TextAlignment.CENTER);
        //text.setFont(Font.font("System", FontWeight.BOLD, FontPosture.REGULAR, 13));
        Button ok = new Button("OK");
        VBox vbox = new VBox(2, text);
        ok.setMinWidth(70);
        vbox.getChildren().add(ok);
        vbox.setMinSize(300, 150);
        vbox.setAlignment(Pos.CENTER);
        //Insets insets = new Insets(20,10,20,10);
        //vbox.setPadding(insets);
        vbox.setSpacing(20);
        Scene scene = new Scene(vbox, 300, 150);
        stage.setScene(scene);
        stage.setTitle("Error Message");
        stage.show();

        ok.setOnMouseClicked(event -> stage.close());
    }
}
