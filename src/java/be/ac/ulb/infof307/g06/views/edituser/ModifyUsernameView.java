package be.ac.ulb.infof307.g06.views.edituser;

import be.ac.ulb.infof307.g06.models.Database;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

/**
 * Create a page for modifying the username of the user.
 */
public class ModifyUsernameView extends ModifyUserView {
    private int userID;
    private Database database;
    private TextField usernameField;

    public ModifyUsernameView(int userID) {
        this.userID = userID;
        database = new Database();
        initLayout();
    }

    //Getters and setters.
    public String getUsernameFieldText() { return usernameField.getText(); }

    /**
     * Initialize the general layout for modifying the username.
     */
    private void initLayout() {
        generalVBox = new VBox(20);
        generalVBox.setAlignment(Pos.CENTER);
        generalVBox.setPrefWidth(200);
        String[] data = database.getInfoUser(userID);
        Label usernameLabel = new Label("Current username : " + data[0]);
        usernameField = new TextField("New username");
        initButtons();
        generalVBox.getChildren().addAll(usernameLabel, usernameField, buttonsHBox);
        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(generalVBox);
        setScene(new Scene(borderPane, 600,600));
    }
}
