package be.ac.ulb.infof307.g06.views;

import be.ac.ulb.infof307.g06.controllers.listeners.MainMenuListener;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

import java.io.FileNotFoundException;

/**
 * This class displays the first window of the program, where the user
 * can select to load a project or create a new one
 */
public class MainMenuView {
    private final MenuBarView menuBarView;
    private MainMenuListener listener;
    private Scene scene;
    private BorderPane borderPane;

    public MainMenuView(MenuBarView menuBarView) { // temp for now views are passed as argument, will change when fxml is refacto for all view
        this.menuBarView = menuBarView;
        initLayout();
    }

    public void closeWindow(ActionEvent event) {
        listener.exitButtonClicked();
    }


    /**
     * Initiate all the buttons and place them inside the BorderPane.
     */
    private void initLayout() {
        VBox centerMenuVBox = new VBox(20);
        centerMenuVBox.setAlignment(Pos.CENTER);
        centerMenuVBox.setPrefWidth(200);
        Button createProjectButton = new Button("Create Project");
        createProjectButton.setOnAction(this::createProject);
        createProjectButton.setMinWidth(centerMenuVBox.getPrefWidth());

        Button openProjectButton = new Button("Open Project");
        openProjectButton.setMinWidth(centerMenuVBox.getPrefWidth());
        openProjectButton.setOnAction(this::openProject);

        Button editUserButton = new Button("Edit User Profile");
        editUserButton.setMinWidth(centerMenuVBox.getPrefWidth());
        editUserButton.setOnAction(this::editUser);

        Button exitButton = new Button("Exit");
        exitButton.setMinWidth(centerMenuVBox.getPrefWidth());
        exitButton.setOnAction(this::closeWindow);
        centerMenuVBox.getChildren().addAll(createProjectButton, openProjectButton, editUserButton, exitButton);

        borderPane = new BorderPane();
        borderPane.setTop(menuBarView);
        borderPane.setCenter(centerMenuVBox);
        scene = new Scene(borderPane, 600, 600);
    }

    /**
     * use the controller to handle this event, it will create a new project
     * @param event on user click
     * @throws FileNotFoundException
     */
    private void createProject(ActionEvent event) {
        listener.createProjectButtonClicked();
    }

    /**
     * Controller method used to initLayout a new View for project management
     * @param event on user click
     */
    private void openProject(ActionEvent event) {
        listener.openProjectButtonClicked();
    }

    /**
     * Open a new scene with able to edit the current user logged in.
     * @param event on user click.
     */
    private void editUser(ActionEvent event) { listener.editUserButtonClicked(); }

    public Scene getScene() {
        return scene;
    }

    public void setListener(MainMenuListener listener) {
        this.listener = listener;
    }
}
