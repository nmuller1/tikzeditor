package be.ac.ulb.infof307.g06.views.drawing;

import be.ac.ulb.infof307.g06.controllers.listeners.ConsoleListener;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;

public class ConsoleView extends StackPane {
    private final Scene scene;
    private static CodeArea codeArea;
    private ConsoleListener listener;

    public ConsoleView() {
        initCodeArea();
        scene = new Scene(this);
    }

    public void addCode(String text) {
        int len = codeArea.getLength();
        String oldText = codeArea.getText();
        if (len != 0) {
            oldText += "\n";
        }
        String newText = oldText + text;
        codeArea.replaceText(0, len, newText);
    }

    /**
     * Construct the code area.
     */
    private void initCodeArea() {
        codeArea = new CodeArea();
        VirtualizedScrollPane virtualizedScrollPane = new VirtualizedScrollPane<>(codeArea);
        getChildren().add(virtualizedScrollPane);
        getStylesheets().add(ConsoleView.class.getResource("/styling/tikz-keyword.css").toExternalForm());
        codeArea.setParagraphGraphicFactory(LineNumberFactory.get(codeArea));
        codeArea.textProperty().addListener((obs, oldText, newText) -> codeArea.setStyleSpans(0, listener.highlightText(newText)));
    }

    public String getText() {
        return codeArea.getText();
    }

    public static CodeArea getCodeArea() {
        return codeArea;
    }

    /**
     * sets the listener
     */
    public void setListener(ConsoleListener listener) {
        this.listener = listener;
    }
}