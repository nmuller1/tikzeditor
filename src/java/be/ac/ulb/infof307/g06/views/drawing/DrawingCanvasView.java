package be.ac.ulb.infof307.g06.views.drawing;

import be.ac.ulb.infof307.g06.controllers.listeners.DrawingListener;
import be.ac.ulb.infof307.g06.models.drawing.ClickStack;
import be.ac.ulb.infof307.g06.models.drawing.DiagramShapeModel;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * Canvas used to draw shapes and lines on
 */
public class DrawingCanvasView extends Pane {
    private final GraphicsContext graphicsContext;
    private final ClickStack clickStack;
    private final Canvas canvas;
    private DrawingListener listener;
    private Color shapeColor;
    private String shapeType;
    private Integer shapeSize;

    public DrawingCanvasView() {
        setWidth(2000);
        setHeight(2000);
        canvas = new Canvas();
        getChildren().add(canvas);
        canvas.widthProperty().bind(widthProperty());
        canvas.heightProperty().bind(heightProperty());
        graphicsContext = canvas.getGraphicsContext2D();
        clickStack = new ClickStack();
        setOnMouseClicked(this::drawOnClick);
    }

    /**
     * @param e click event
     * @return coordinates where the user clicked within the canvas
     */
    public Double[] getClickPosition(MouseEvent e) {
        double x = e.getX();
        double y = e.getY();
        return new Double[]{x, y};
    }

    /**
     * @param lineType          arc, dash or edge
     * @param shapeColor        color to draw
     * @param size              of the shape
     * @param firstCoordinates  of click
     * @param secondCoordinates of click
     * @return a shape formed by given parameters
     */
    public DiagramShapeModel createShape(String lineType, Color shapeColor, Integer size, Double[] firstCoordinates, Double[] secondCoordinates) {
        return listener.createShape(lineType, shapeColor, size, firstCoordinates, secondCoordinates);
    }

    /**
     * @param lineType    arc, dash or edge
     * @param shapeColor  color to draw
     * @param size        of the shape
     * @param coordinates of click
     * @return a shape formed by given parameters
     */
    public DiagramShapeModel createShape(String lineType, Color shapeColor, Integer size, Double[] coordinates) {
        return listener.createShape(lineType, shapeColor, size, coordinates);
    }

    /**
     * Writes the code of given shape in editor
     * @param shape to be translated
     */
    private void addTikZCodeFrom(DiagramShapeModel shape) {
        listener.addTikZCodeFrom(shape);
    }

    /**
     * Draws the shape according to selected parameters.
     * @return the shape drawn
     */
    private DiagramShapeModel drawShapeWithTwoClicks() {
        Double[] secondCoordinates = clickStack.pop();
        Double[] firstCoordinates = clickStack.pop();
        return createShape(shapeType, shapeColor, shapeSize, firstCoordinates, secondCoordinates);
    }

    /**
     * Draws the shape according to selected parameters.
     * @return the shape drawn
     */
    private DiagramShapeModel drawShapeWithOneClick() {
        Double[] coordinates = clickStack.pop();
        DiagramShapeModel shape = createShape(shapeType, shapeColor, shapeSize, coordinates);
        shape.draw(getGraphicsContext());
        return shape;
    }

    /**
     * Initializes the behaviour of Canvas when a mouse click is performed
     */
    private void drawOnClick(MouseEvent e) {
        clickStack.push(getClickPosition(e));
        getDrawingInfo();
        if (isDrawingTwoClick()) {
            if (clickStack.size() == 2) {
                DiagramShapeModel shape = drawShapeWithTwoClicks();
                addTikZCodeFrom(shape);
            }
        } else {
            DiagramShapeModel shape = drawShapeWithOneClick();
            addTikZCodeFrom(shape);
        }
    }

    private void getDrawingInfo() {
        shapeColor = listener.getShapeColor();
        shapeType = listener.getShapeType();
        shapeSize = listener.getShapeSize();
    }

    private boolean isDrawingTwoClick() {
        return shapeType.equals("edge") || shapeType.equals("arc") || shapeType.equals("dash");
    }

    /**
     * @return GraphicsContext object
     */
    public GraphicsContext getGraphicsContext() {
        return graphicsContext;
    }

    /**
     * @return Canvas object
     */
    public Canvas getCanvas() { return canvas; }

    /**
     * Set the listener for the view
     * @param listener Listener for the view
     */
    public void setListener(DrawingListener listener) {
        this.listener = listener;
    }


}