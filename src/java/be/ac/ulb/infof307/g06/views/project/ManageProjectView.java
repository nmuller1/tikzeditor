package be.ac.ulb.infof307.g06.views.project;

import be.ac.ulb.infof307.g06.controllers.listeners.ManageProjectListener;
import be.ac.ulb.infof307.g06.models.FileInfo;
import be.ac.ulb.infof307.g06.views.MenuBarView;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.List;

/**
 * Class that displays the management option for the files of the user.
 */
public class ManageProjectView extends BorderPane {
    private final Scene scene;
    private final MenuBarView menuBarView;
    private ManageProjectListener listener;
    private TableView tableView;
    private TableView.TableViewSelectionModel selectionModel;
    private HBox manageProjectHBox;

    /**
     * constructor of the ManageProjectView class.
     * @param menuBarView
     */
    public ManageProjectView(MenuBarView menuBarView) {
        this.menuBarView = menuBarView;
        initLayout();
        scene = new Scene(this);
        scene.setRoot(this);
    }

    /**
     * Method that collect all the files of the user and displays them in a table.
     */
    public void setFileTable() {
        tableView = new TableView();
        List<FileInfo> listOfFiles = listener.getUserFileInfo();
        for (FileInfo listOfFile : listOfFiles) {
            tableView.getItems().add(listOfFile);
        }
        //Add the columns to the table.
        TableColumn<String, FileInfo> column1 = new TableColumn<>("ProjectModel Name");
        column1.setCellValueFactory(new PropertyValueFactory<>("projectName"));

        TableColumn<String, FileInfo> column2 = new TableColumn<>("Owner Name");
        column2.setCellValueFactory(new PropertyValueFactory<>("ownerName"));

        TableColumn<String, FileInfo> column3 = new TableColumn<>("Last modification");
        column3.setCellValueFactory(new PropertyValueFactory<>("lastModification"));

        tableView.getColumns().add(column1);
        tableView.getColumns().add(column2);
        tableView.getColumns().add(column3);

        selectionModel = tableView.getSelectionModel();

        selectionModel.setSelectionMode(SelectionMode.MULTIPLE);
        setCenter(new VBox(manageProjectHBox, tableView));
    }

    /**
     * PopUpWindow that is shown if various files are selected when rename or edit file is clicked.
     */
    public void showAlertMoreThanOneFile() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText("Please select only one file!");
        alert.showAndWait();
    }

    /**
     * Initiate buttons and add them to HBox layout.
     */
    private void initLayout() {
        manageProjectHBox = new HBox();
        Button openProject = new Button("Open");
        openProject.setOnAction(this::openFile);
        Button copyButton = new Button("Copy");
        copyButton.setOnAction(this::copyFile);
        Button deleteButton = new Button("Delete");
        deleteButton.setOnAction(this::deleteFile);
        Button newDirButton = new Button("Save in new directory");
        newDirButton.setOnAction(this::changeFileDir);
        Button renameButton = new Button("Rename");
        renameButton.setOnAction(this::renameFile);
        Button returnMenuButton = new Button("Return");
        returnMenuButton.setOnAction(this::returnMenu);
        manageProjectHBox.getChildren().addAll(openProject, copyButton, deleteButton, newDirButton, renameButton, returnMenuButton);
        manageProjectHBox.setSpacing(20);
        setTop(menuBarView);
        setCenter(manageProjectHBox);
    }

    private void copyFile(ActionEvent event) {
        listener.copyProject();
        selectionModel.clearSelection();
    }

    private void deleteFile(ActionEvent event) {
        listener.deleteProject();
        selectionModel.clearSelection();
    }

    private void changeFileDir(ActionEvent event) {
        listener.changeDirectory();
        selectionModel.clearSelection();
    }

    private void renameFile(ActionEvent event) {
        listener.renameProject();
        selectionModel.clearSelection();
    }

    private void openFile(ActionEvent event) {
        listener.openProject();
        selectionModel.clearSelection();
    }

    private void returnMenu(ActionEvent event) {
        listener.returnToMainWindow();
    }

    /**
     * Getter to get the current index selected.
     * @return int the current index selected.
     */
    public int getCurrentSelectedIndex() {
        return tableView.getSelectionModel().getSelectedIndex();
    }

    /**
     * Getter to get the FileInfo instances selected on the table.
     * @return ObservableList of FileInfo, the FileInfo instances selected on the table.
     */
    public ObservableList<FileInfo> getSelectedItems() {
        return selectionModel.getSelectedItems();
    }

    public void setListener(ManageProjectListener listener) {
        this.listener = listener;
    }
}
