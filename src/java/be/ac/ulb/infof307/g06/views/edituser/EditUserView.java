package be.ac.ulb.infof307.g06.views.edituser;

import be.ac.ulb.infof307.g06.controllers.listeners.EditUserListener;
import be.ac.ulb.infof307.g06.models.Database;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * General user profile editing page.
 * Can modify username, email and password.
 */
public class EditUserView {
    private EditUserListener listener;
    private Scene scene;
    private VBox generalVBox;
    private HBox usernameHBox;
    private HBox emailHBox;
    private HBox passwordHBox;
    private int userID;
    private Database database;
    private String[] data;

    public EditUserView(int userID) {
        this.userID = userID;
        database = new Database();
        initLayout();
    }

    //Getters and setters.
    public void setListener(EditUserListener listener) { this.listener = listener; }
    public Scene getScene() {return scene;}

    /**
     * Initialize the layout of the edit user view.
     */
    private void initLayout() {
        data = database.getInfoUser(userID);
        usernameHBox = initLabelButton("Username : " + data[0], this::modifyUsername);
        emailHBox = initLabelButton("Email : " + data[3], this::modifyEmail);
        passwordHBox = initLabelButton("Password : *****", this::modifyPassword);
        finalInit();
    }

    /**
     *  Initialize a Hbox with a label and a button to modify.
     * @param infoLabel info displayed next to the button.
     * @param method that will be called when the modify button is clicked.
     * @return HBox that needs to be added to the general layout.
     */
    private HBox initLabelButton(String infoLabel, EventHandler<ActionEvent> method) {
        HBox newHBox = new HBox(20);
        newHBox.setAlignment(Pos.CENTER);
        newHBox.setPrefWidth(200);
        Label label = new Label(infoLabel);
        Button button = new Button("Modify");
        button.setMinWidth(newHBox.getPrefWidth());
        button.setOnAction(method);
        newHBox.getChildren().addAll(label, button);
        return newHBox;
    }

    /**
     * Create the general VBox and assemble all the HBox created earlier.
     */
    private void finalInit() {
        generalVBox = new VBox(20);
        generalVBox.setAlignment(Pos.CENTER);
        generalVBox.setPrefWidth(200);
        Button exitButton = new Button("Exit");
        exitButton.setOnAction(this::exit);
        generalVBox.getChildren().addAll(usernameHBox, emailHBox, passwordHBox, exitButton);
        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(generalVBox);
        scene = new Scene(borderPane, 600,600);
    }

    /**
     * when exit button is clicked
     * @param event
     */
    private void exit(ActionEvent event) { listener.exitButtonClicked(); }

    /**
     * when modifyUsername button is clicked
     * @param event
     */
    private void modifyUsername(ActionEvent event) {listener.modifyUsernameButtonClicked();}

    /**
     * when modifyEmail button is clicked
     * @param event
     */
    private void modifyEmail(ActionEvent event) {listener.modifyEmailButtonClicked();}

    /**
     * when modifyPassword is clicked
     * @param event
     */
    private void modifyPassword(ActionEvent event) {listener.modifyPasswordButtonClicked();}
}
