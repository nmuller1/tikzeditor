package be.ac.ulb.infof307.g06.views;

import be.ac.ulb.infof307.g06.controllers.listeners.SignInListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

import java.io.IOException;

/**
 * view for sign in menu; allowing the user to log into the application
 */
public class SignInView extends HBox {
    @FXML
    private TextField usernameTextField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Button confirmButton;
    @FXML
    private Hyperlink signUpHyperLink;

    private final Scene scene;
    private SignInListener listener;

    public SignInView() {
        scene = new Scene(this);
        loadFxml();
    }

    @FXML
    /**
     * called when user tries to log in
     */
    public void confirmButtonClicked(ActionEvent event){
        String username = usernameTextField.getText();
        String password = passwordField.getText();
        listener.confirmButtonClicked(username,password);
    }

    @FXML
    /**
     * link to the singUp menu
     */
    public void signUpHyperlinkClicked(ActionEvent event) {
        listener.signUpHyperlinkClicked();
    }

    private void loadFxml() {
        FXMLLoader fxmlLoader = new FXMLLoader(SignInView.class.getResource("/fxml/SignInView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setControllerFactory(SignInView -> this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void setListener(SignInListener listener) {
        this.listener = listener;
    }
}
