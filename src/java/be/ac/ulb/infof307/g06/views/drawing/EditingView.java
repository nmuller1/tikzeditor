package be.ac.ulb.infof307.g06.views.drawing;

import be.ac.ulb.infof307.g06.views.MenuBarView;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

/**
 * View of the editing scene.
 */
public class EditingView extends BorderPane {
    private final Scene scene;
    private final ToolBarView toolBarView;
    private final MenuBarView menuBarView;
    private final ConsoleView consoleView;
    private final DrawingCanvasView drawingCanvasView;
    private SplitPane splitPane;

    public EditingView(ConsoleView consoleView, DrawingCanvasView drawingCanvasView, ToolBarView toolBarView, MenuBarView menuBarView) { // temp for now view are passed as setter, will change when fxml is refacto for all view
        this.consoleView = consoleView;
        this.drawingCanvasView = drawingCanvasView;
        this.toolBarView = toolBarView;
        this.menuBarView = menuBarView;
        initLayout();
        scene = new Scene(this);
        scene.setRoot(this);
    }

    private void initLayout() {
        splitPane = new SplitPane();
        splitPane.getItems().addAll(drawingCanvasView, consoleView);
        setTop(menuBarView);
        setCenter(new VBox(toolBarView, splitPane));
    }
}
