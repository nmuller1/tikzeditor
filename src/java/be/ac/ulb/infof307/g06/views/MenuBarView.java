package be.ac.ulb.infof307.g06.views;

import be.ac.ulb.infof307.g06.controllers.listeners.MenuBarListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * The menu displayed on the top of the edition window
 */
public class MenuBarView extends MenuBar {
    @FXML
    private MenuItem openProjectMenuItem;

    @FXML
    private MenuItem saveProjectMenuItem;

    @FXML
    private MenuItem closeProjectMenuItem;

    @FXML
    private MenuItem importProjectMenuItem;

    @FXML
    private MenuItem exportProjectMenuItem;

    @FXML
    private MenuItem exitMenuItem;

    @FXML
    private MenuItem helpMenuItem;

    @FXML
    private MenuItem aboutMenuItem;

    private MenuBarListener listener;

    public MenuBarView() {
        loadFxml();
    }

    @FXML
    /**
     * when exit button is clicked
     */
    public void exitMenuItemOnClick(ActionEvent event) {
        listener.exitMenuItemClicked();
    }

    @FXML
    /**
     * when help button is clicked
     */
    public void helpMenuItemOnClick(ActionEvent event) {
        listener.helpMenuItemClicked();
    }

    @FXML
    /**
     * when about is clicked
     */
    public void aboutMenuItemOnClick(ActionEvent event) {
        listener.aboutMenuItemClicked();
    }

    @FXML
    /**
     * when close project is clicked
     */
    public void closeProjectMenuItemOnClick(ActionEvent event) {
        listener.closeProject();
    }

    @FXML
    /**
     * when open project is clicked
     */
    public void openProjectMenuItemOnClick(ActionEvent event) {

    }

    @FXML
    /**
     * when save project is clicked
     */
    public void saveProjectMenuItemOnClick(ActionEvent event) {
        listener.saveProject();
    }

    @FXML
    /**
     * when import project is clicked
     */
    public void importProjectMenuItemOnClick(ActionEvent event){
        listener.importProjectButtonClicked();
    }

    @FXML
    /**
     * when export project is clicked
     */
    public void exportProjectMenuItemOnClick(ActionEvent event) {
        listener.exportProjectButtonClicked();
    }

    /**
     * Asks the user a path
     * @param primaryStage to show the fileChooser window
     * @return the selected path
     */
    public String getPath(Stage primaryStage) {
        return new FileChooserView("Export In", "All Files", "*.*").showSaveDialogInStage(primaryStage).getAbsolutePath();
    }

    /**
     * load and prepare fxml
     */
    private void loadFxml() {
        FXMLLoader fxmlLoader = new FXMLLoader(MenuBarView.class.getResource("/fxml/MenuBarView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setControllerFactory(MenuBarView -> this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    /**
     * set what is visible depending on what was called
     * @param mode indication on what was called
     */
    public void setParentView(String mode) {
        if (mode.equals("MainMenu") || mode.equals("ManageProject")) {
            openProjectMenuItem.setVisible(false);
            saveProjectMenuItem.setVisible(false);
            closeProjectMenuItem.setVisible(false);
            importProjectMenuItem.setVisible(false);
            exportProjectMenuItem.setVisible(false);
        } else if (mode.equals("Editing")) {
            openProjectMenuItem.setVisible(true);
            saveProjectMenuItem.setVisible(true);
            closeProjectMenuItem.setVisible(true);
            importProjectMenuItem.setVisible(true);
            exportProjectMenuItem.setVisible(true);
        }
    }

    /**
     * set listener
     * @param listener
     */
    public void setListener(MenuBarListener listener) {
        this.listener = listener;
    }

}
