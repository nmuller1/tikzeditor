package be.ac.ulb.infof307.g06.controllers.drawing;

import be.ac.ulb.infof307.g06.controllers.MenuBarController;
import be.ac.ulb.infof307.g06.controllers.PreviewController;
import be.ac.ulb.infof307.g06.controllers.listeners.*;
import be.ac.ulb.infof307.g06.controllers.versionning.RevertController;
import be.ac.ulb.infof307.g06.models.Database;
import be.ac.ulb.infof307.g06.models.drawing.DiagramShapeModel;
import be.ac.ulb.infof307.g06.models.importExport.ExportModel;
import be.ac.ulb.infof307.g06.models.importExport.ImportModel;
import be.ac.ulb.infof307.g06.models.UserInfoModel;
import be.ac.ulb.infof307.g06.views.drawing.EditingView;
import be.ac.ulb.infof307.g06.views.ErrorMessage;
import be.ac.ulb.infof307.g06.views.FileChooserView;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;


/**
 * Class combining every tool needed in project edition
 */
public class EditingController implements MenuBarEditingControllerListener, ToolBarControllerListener, DrawingControllerListener, ConsoleControllerListener, RevertControllerListener {
    private final Stage primaryStage;
    private final EditingView editingView;
    private final int projectID;
    private final Database database;
    private final String content;
    private final UserInfoModel userInfoModel;
    private ConsoleController consoleController;
    private DrawingController drawingController;
    private ToolBarController toolBarController;
    private MenuBarController menuBarController;
    private PreviewController previewController;
    private RevertController revertController;

    public EditingController(Stage primaryStage, String content, int currentProjectID, UserInfoModel userInfoModel) {
        this.primaryStage = primaryStage;
        this.content = content;
        this.userInfoModel = userInfoModel;
        initControllers();
        editingView = new EditingView(consoleController.getView(), drawingController.getView(), toolBarController.getView(), menuBarController.getView());
        database = new Database();
        projectID = currentProjectID;
    }

    @Override
    public void saveProject() {
        database.saveProject(projectID, consoleController.getText());
    }

    @Override
    public void importProjectButtonClicked() {
        FileChooserView fileChooserView = new FileChooserView("Import project", "Tar files", "*.tar.gz");
        File compressedFile = fileChooserView.showOpenDialogInStage(primaryStage);
        FileChooserView destinationFolderChooserView = new FileChooserView("Save destination", "Tex files", ".tex");
        File folder = destinationFolderChooserView.showSaveDialogInStage(primaryStage);
        String outputPath = folder.getPath();
        ImportModel importModel = new ImportModel(userInfoModel);

        try {
            importModel.importProject(compressedFile, outputPath);
        } catch (IOException e){
            new ErrorMessage(primaryStage, "An error occured while trying to decompress\n" +
                    "Error : "+e);
        }

    }

    @Override
    public void exportProjectButtonClicked() {
        String[] projectData = database.getProjectData(projectID);
        FileChooserView fileChooserView = new FileChooserView("Export project", "Tar files", ".tar.gz");
        File file = fileChooserView.showSaveDialogInStage(primaryStage);
        File projectFile = new File(projectData[1]);
        try {
            ExportModel.exportProject(file.getAbsolutePath(), projectFile);
        } catch (IOException e) {
            new ErrorMessage(primaryStage, "An error occured while trying to compress");
        }
    }

    @Override
    public void translate() {
        drawingController.clear();
        consoleController.translate();
        saveProject();
    }

    @Override
    public void clear() {
        drawingController.clear();
    }

    @Override
    public void preview(){
        try{
            previewController.preview();
        } catch (IOException e){
            new ErrorMessage(primaryStage,"An error has occurred during the pdf creation process\n" +
                    "Error : " + e);
        }

    }

    @Override
    public void addTikZCodeFrom(DiagramShapeModel shape) {
        consoleController.setText(shape.getTikZCode());
    }

    /**
     * Draws the given rectangle
     * @param type        the type
     * @param color       of the outline
     * @param size        of the outline
     * @param coordinates of the rectangle
     */
    @Override
    public void drawRectangle(String type, Color color, Integer size, Double[][] coordinates) {
        drawingController.drawRectangle(type, color, size, coordinates);
    }

    /**
     * Draws the given circle
     * @param color      of the outline
     * @param size       of the outline
     * @param coordinate of the center
     */
    @Override
    public void drawCircle(Color color, Integer size, Double[][] coordinate) {
        drawingController.drawCircle(color, coordinate[1][0].intValue(), coordinate);
    }

    /**
     * Draws the given shape
     * @param type             of the outline
     * @param color            of the outline
     * @param size             of the outline
     * @param firstCoordinate  of the shape
     * @param secondCoordinate of the shape
     */
    @Override
    public void drawShape(String type, Color color, Integer size, Double[] firstCoordinate, Double[] secondCoordinate) {
        drawingController.drawShape(type, color, size, firstCoordinate, secondCoordinate);
    }

    /**
     *Method that update the console and canvas content with a given content.
     * @param content String of the content to update the console and the canvas.
     */
    @Override
    public void updateConsoleCanvas(String content){
        consoleController.setText(content);
        drawingController.clear();
        consoleController.translate();
        saveProject();
    }

    /**
     * Fills the given rectangle
     * @param colorFill   to fill
     * @param size        of the rectangle
     * @param coordinates of the rectangle
     */
    public void fillRectangle(Color colorFill, Integer size, Double[][] coordinates) {
        drawingController.fillRectangle(colorFill, size, coordinates);
    }

    /**
     * Fills the given circle
     * @param colorFill   to fill
     * @param size        of the circle
     * @param coordinates of the center
     */
    public void fillCircle(Color colorFill, Integer size, Double[][] coordinates) {
        drawingController.fillCircle(colorFill, coordinates[1][0].intValue(), coordinates);
    }

    /**
     * Displays the linked view
     */
    public void show() {
        primaryStage.setTitle(database.getProjectData(projectID)[0]);
        primaryStage.setScene(editingView.getScene());
        primaryStage.setMaximized(true);
        primaryStage.show();
    }

    /**
     * Initializes all the controllers and the views for the editingView.
     * Display the editingView afterwards.
     */
    private void initControllers() {
        drawingController = new DrawingController();
        drawingController.setListener(this);
        consoleController = new ConsoleController(content);
        consoleController.setListener(this);
        consoleController.translate();
        toolBarController = new ToolBarController();
        toolBarController.setListener(this);
        menuBarController = new MenuBarController(primaryStage, "Editing", userInfoModel);
        menuBarController.setListener(this);
        previewController = new PreviewController(userInfoModel, drawingController);
        revertController = new RevertController();
        revertController.setListener(this);
    }

    /**
     * @return the size of the shape from DrawingCanvasView toolbar
     */
    @Override
    public Integer getShapeSize() {
        return toolBarController.getShapeSize();
    }

    /**
     * @return the color from DrawingCanvasView toolBar
     */
    @Override
    public Color getShapeColor() {
        return toolBarController.getShapeColor();
    }

    /**
     * @return the shape from DrawingCanvasView toolBar
     */
    @Override
    public String getShapeType() {
        return toolBarController.getShapeType();
    }
}
