package be.ac.ulb.infof307.g06.controllers.listeners;

public interface ToolBarListener {
    void translateButtonClicked();
    void clearButtonClicked();
    void previewButtonClicked();
}
