package be.ac.ulb.infof307.g06.controllers.listeners;

public interface EditUserListener {
    void exitButtonClicked();
    void modifyUsernameButtonClicked();
    void modifyEmailButtonClicked();
    void modifyPasswordButtonClicked();
}
