package be.ac.ulb.infof307.g06.controllers;

import be.ac.ulb.infof307.g06.Main;
import be.ac.ulb.infof307.g06.controllers.drawing.DrawingController;
import be.ac.ulb.infof307.g06.models.PreviewModel;
import be.ac.ulb.infof307.g06.models.UserInfoModel;
import javafx.application.HostServices;
import java.io.File;
import java.io.IOException;

/**
 * Preview the drawing part of the project in a pdf.
 */
public class PreviewController {
    private final UserInfoModel userInfoModel;
    private final DrawingController drawingController;
    private final PreviewModel previewModel;

    public PreviewController(UserInfoModel userInfoModel, DrawingController drawingController) {
        this.userInfoModel = userInfoModel;
        this.drawingController = drawingController;
        this.previewModel = new PreviewModel(this.drawingController);
    }

    /**
     * Compile the pdf of the project, and shows it
     */
    public void preview() throws IOException {
        String documentName = previewModel.generateFullPDF();
        File file = new File(documentName);
        HostServices hostServices = Main.getInstance().getHostServices();
        hostServices.showDocument(file.getAbsolutePath());
        file.deleteOnExit();
    }
}
