package be.ac.ulb.infof307.g06.controllers.drawing;

import be.ac.ulb.infof307.g06.controllers.listeners.DrawingControllerListener;
import be.ac.ulb.infof307.g06.controllers.listeners.DrawingListener;
import be.ac.ulb.infof307.g06.models.drawing.ArcModel;
import be.ac.ulb.infof307.g06.models.drawing.CircleModel;
import be.ac.ulb.infof307.g06.models.drawing.DashModel;
import be.ac.ulb.infof307.g06.models.drawing.DiagramShapeModel;
import be.ac.ulb.infof307.g06.models.drawing.LineModel;
import be.ac.ulb.infof307.g06.models.drawing.RectangleModel;
import be.ac.ulb.infof307.g06.models.drawing.TriangleModel;
import be.ac.ulb.infof307.g06.views.drawing.DrawingCanvasView;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * Manages the choice and drawing of diagrams
 */
public class DrawingController implements DrawingListener {
    private final DrawingCanvasView drawingCanvasView;
    private DrawingControllerListener listener;

    /**
     * Constructor.
     * Creates the EditingView class such that the user can start drawing.
     */
    public DrawingController() {
        drawingCanvasView = new DrawingCanvasView();
        drawingCanvasView.setListener(this);
    }

    @Override
    public void addTikZCodeFrom(DiagramShapeModel shape) {
        listener.addTikZCodeFrom(shape);
    }

    /**
     * On click to draw initializes the shape to draw with selected options
     * activates draw and translation
     */

    @Override
    public DiagramShapeModel createShape(String lineType, Color shapeColor, Integer size, Double[] coordinates) {
        DiagramShapeModel shape;
        if (lineType.equals("circle")) {
            shape = new CircleModel(lineType, shapeColor, size, coordinates);
        } else if (lineType.equals("rectangle")) {
            shape = new RectangleModel(lineType, shapeColor, size, coordinates);
        } else {
            shape = new TriangleModel(lineType, shapeColor, size, coordinates);
        }
        GraphicsContext gc = getGraphicsContext();
        shape.draw(gc);
        return shape;
    }

    /**
     * Create a new Rectangle
     * @param lineType the outline type
     * @param shapeColor the color of the outline
     * @param size the size of the rectangle
     * @param coordinates of the two opposed edges
     * @return the created rectangle
     */
    @Override
    public RectangleModel createShape(String lineType, Color shapeColor, Integer size, Double[][] coordinates) {
        return new RectangleModel(lineType, shapeColor, size, coordinates);
    }

    /**
     * On click to draw initializes the shape to draw with selected options
     * Works for lines (edges and arcs) and therefore needs a 2nd point
     */

    @Override
    public CircleModel createShape(Color shapeColor, Integer size, Double[] coordinates) {
        return new CircleModel("circle", shapeColor, size, coordinates);
    }

    /**
     * Creates a new diagram shape
     * @param lineType the outline type
     * @param shapeColor the color of the outline
     * @param size the size of the rectangle
     * @param firstCoordinates needed
     * @param secondCoordinates needed
     * @return the created diagram shape
     */
    @Override
    public DiagramShapeModel createShape(String lineType, Color shapeColor, Integer size, Double[] firstCoordinates, Double[] secondCoordinates) {
        DiagramShapeModel shape;
        if (lineType.equals("edge")) {
            shape = new LineModel(lineType, shapeColor, size, firstCoordinates, secondCoordinates);
        } else if (lineType.equals("dash")) {
            shape = new DashModel(lineType, shapeColor, size, firstCoordinates, secondCoordinates);
        } else {
            shape = new ArcModel(lineType, shapeColor, size, firstCoordinates, secondCoordinates);
        }
        GraphicsContext graphicsContext = getGraphicsContext();
        shape.draw(graphicsContext);
        return shape;
    }

    /**
     * @return the shape from DrawingCanvasView toolBar
     */
    @Override
    public String getShapeType() {
        return listener.getShapeType();
    }

    /**
     * @return the color from DrawingCanvasView toolBar
     */
    @Override
    public Color getShapeColor() {
        return listener.getShapeColor();
    }

    /**
     * @return the size of the shape from DrawingCanvasView toolbar
     */
    @Override
    public Integer getShapeSize() {
        return listener.getShapeSize();
    }

    /**
     * Draws a rectangle
     * @param type        of rectangle
     * @param color       of the rectangle
     * @param size        of the rectangle
     * @param coordinates of the rectangle
     */
    public void drawRectangle(String type, Color color, Integer size, Double[][] coordinates) {
        draw(createShape(type, color, size, coordinates));
    }

    /**
     * Fills the given rectangle
     * @param colorFill the color used to fill
     * @param size the size of the rectangle
     * @param coordinates of the rectangle
     */
    public void fillRectangle(Color colorFill, Integer size, Double[][] coordinates) {
        fill(createShape("rectangle", colorFill, size, coordinates));
    }

    /**
     * Draws a circle
     * @param color       of the circle
     * @param size        of the circle
     * @param coordinates of the circle
     */
    public void drawCircle(Color color, Integer size, Double[][] coordinates) {
        draw(createShape(color, size, coordinates[0]));
    }

    /**
     * Fills the given circle
     * @param colorFill the color used to fill
     * @param size the size of the circle
     * @param coordinates of the circle
     */
    public void fillCircle(Color colorFill, Integer size, Double[][] coordinates) {
        fill(createShape(colorFill, size, coordinates[0]));
    }

    /**
     * Draws a shape
     * @param color       of the shape
     * @param size        of the shape
     * @param firstCoordinates of the shape
     * @param secondCoordinates of the shape
     */
    public void drawShape(String type, Color color, Integer size, Double[] firstCoordinates, Double[] secondCoordinates) {
        draw(createShape(type, color, size, firstCoordinates, secondCoordinates));
    }

    /**
     * Draw a shape in the graphic context
     * @param shape to be drawn
     */
    public void draw(DiagramShapeModel shape) {
        shape.draw(getGraphicsContext());
    }

    /**
     * Fills the given rectangle
     * @param shape the rectangle to fill
     */
    public void fill(RectangleModel shape) {
        shape.fill(getGraphicsContext());
    }

    /**
     * Fills the given circle
     * @param shape the circle to fill
     */
    public void fill(CircleModel shape) {
        shape.fill(getGraphicsContext());
    }

    /**
     * Clears the drawing canvas
     */
    public void clear() {
        drawingCanvasView.getGraphicsContext().clearRect(0, 0, 2000, 2000);
    }

    /**
     * @return the view associated to the current controller
     */
    public DrawingCanvasView getView() {
        return drawingCanvasView;
    }

    /**
     * @return the graphicsContext associated to the view
     */
    public GraphicsContext getGraphicsContext() {
        return drawingCanvasView.getGraphicsContext();
    }

    /**
     * @return the canvas associated to the view
     */
    public Canvas getCanvas() { return drawingCanvasView.getCanvas(); }

    /**
     * @param listener to link controller and view
     */
    public void setListener(DrawingControllerListener listener) {
        this.listener = listener;
    }
}



