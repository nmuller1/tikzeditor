package be.ac.ulb.infof307.g06.controllers.listeners;

public interface ModifyUserListener {
    void confirmButtonClicked();
    void cancelButtonClicked();
}
