package be.ac.ulb.infof307.g06.controllers.project;

import be.ac.ulb.infof307.g06.controllers.listeners.RenameProjectListener;
import be.ac.ulb.infof307.g06.models.FileInfo;
import be.ac.ulb.infof307.g06.models.ManageProjectModel;
import be.ac.ulb.infof307.g06.views.ErrorMessage;
import be.ac.ulb.infof307.g06.views.project.RenameProjectView;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class RenameProjectController implements RenameProjectListener {
    private final Stage primaryStage;
    private final Stage stage;
    private final RenameProjectView renameProjectView;
    private final ManageProjectModel model;

    public RenameProjectController(Stage primaryStage, ManageProjectModel model, FileInfo fileInfo) {
        this.primaryStage = primaryStage;
        this.model = model;
        renameProjectView = new RenameProjectView();
        renameProjectView.setFileInfo(fileInfo);
        renameProjectView.setListener(this);
        stage = new Stage();
    }

    /**
     * Call the model to rename the file.
     * @param newName  new name.
     * @param fileInfo absolute path of the old name.
     */
    @Override
    public void renameFile(String newName, FileInfo fileInfo) {
        if(!model.renameProject(newName, fileInfo)) {
            new ErrorMessage(primaryStage,"Error : name already exists");
        }
    }

    /**
     * Close rename stage
     */
    @Override
    public void closeWindow() {
        stage.close();
    }

    /**
     * Displays the associated view
     */
    public void show() {
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(primaryStage);
        stage.setTitle("Rename project");
        stage.setResizable(false);
        stage.setScene(renameProjectView.getScene());
        stage.showAndWait();
    }
}
