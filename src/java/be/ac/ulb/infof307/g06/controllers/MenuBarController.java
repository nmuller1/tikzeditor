package be.ac.ulb.infof307.g06.controllers;

import be.ac.ulb.infof307.g06.controllers.listeners.MenuBarEditingControllerListener;
import be.ac.ulb.infof307.g06.controllers.listeners.MenuBarListener;
import be.ac.ulb.infof307.g06.models.UserInfoModel;
import be.ac.ulb.infof307.g06.views.MenuBarView;
import javafx.application.Platform;
import javafx.stage.Stage;

/**
 * Controller of the top's Bar Window
 */
public class MenuBarController implements MenuBarListener {
    private final Stage primaryStage;
    private final MenuBarView view;
    private final UserInfoModel userInfoModel;
    private MenuBarEditingControllerListener listener;

    public MenuBarController(Stage primaryStage, String parentView, UserInfoModel userInfoModel) {
        this.primaryStage = primaryStage;
        this.userInfoModel = userInfoModel;
        view = new MenuBarView();
        view.setParentView(parentView);
        view.setListener(this);
    }

    /**
     * Exits the program
     */
    @Override
    public void exitMenuItemClicked() {
        Platform.exit();
    }

    /**
     * Displays the about menu
     */
    @Override
    public void aboutMenuItemClicked() {
    }

    /**
     * Displays the help menu
     */
    @Override
    public void helpMenuItemClicked() {
        HelpController helpController = new HelpController(primaryStage);
        helpController.show();
    }

    /**
     * Saves the current project
     */
    @Override
    public void saveProject() {
        listener.saveProject();
    }

    /**
     * Closes the current project
     */
    @Override
    public void closeProject() {
        MainMenuController mainMenuController = new MainMenuController(primaryStage, userInfoModel);
        mainMenuController.show();
    }

    /**
     * Imports the project from .tar.gz
     */
    @Override
    public void importProjectButtonClicked() {
        listener.importProjectButtonClicked();
    }

    /**
     * Exports the project in .tar.gz
     */
    @Override
    public void exportProjectButtonClicked() {
        listener.exportProjectButtonClicked();
    }

    /**
     * Asks the user a path to export the project
     * @return the selected path
     */
    @Override
    public String getPath() {
        String path = view.getPath(primaryStage);
        return path;
    }

    /**
     * @return the associated view
     */
    public MenuBarView getView() {
        return view;
    }

    /**
     * @param listener the link between view and controller
     */
    public void setListener(MenuBarEditingControllerListener listener) {
        this.listener = listener;
    }
}
