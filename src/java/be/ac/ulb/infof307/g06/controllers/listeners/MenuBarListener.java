package be.ac.ulb.infof307.g06.controllers.listeners;


public interface MenuBarListener {
    void exitMenuItemClicked();
    void aboutMenuItemClicked();
    void helpMenuItemClicked();
    void saveProject();
    void closeProject();
    void importProjectButtonClicked();
    void exportProjectButtonClicked();
    String getPath();
}
