package be.ac.ulb.infof307.g06.controllers.listeners;

import be.ac.ulb.infof307.g06.views.MenuBarView;

public interface MainMenuListener {
    void createProjectButtonClicked();
    void openProjectButtonClicked();
    void editUserButtonClicked();
    void exitButtonClicked();
}
