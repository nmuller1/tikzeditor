package be.ac.ulb.infof307.g06.controllers.edituser;

import be.ac.ulb.infof307.g06.controllers.listeners.ModifyUserListener;
import be.ac.ulb.infof307.g06.models.AccountModel;
import be.ac.ulb.infof307.g06.models.Database;
import be.ac.ulb.infof307.g06.views.ErrorMessage;
import be.ac.ulb.infof307.g06.views.edituser.ModifyEmailView;
import javafx.stage.Stage;

/**
 * Create a new page to modify the email of the user.
 * Verify the infos before modifying.
 */
public class ModifyEmailController implements ModifyUserListener {
    private final ModifyEmailView modifyEmailView;
    private final Stage primaryStage;
    private final int userID;

    public ModifyEmailController(Stage primaryStage, int userID) {
        this.primaryStage = primaryStage;
        this.userID = userID;
        modifyEmailView = new ModifyEmailView(userID);
        modifyEmailView.setListener(this);
    }

    @Override
    /**
     * Try to modify the email.
     */
    public void confirmButtonClicked() {
        updatingEmail();
    }

    @Override
    /**
     * Return to the edit user page.
     */
    public void cancelButtonClicked() {
        EditUserController editUserController = new EditUserController(primaryStage, userID);
        editUserController.show();
    }

    /**
     * Show the modifyEmailView.
     */
    public void show() {
        primaryStage.setScene(modifyEmailView.getScene());
        primaryStage.show();
    }

    /**
     * Verify if the email is valid and available, then modify it.
     * Return to the edit user page.
     */
    private void updatingEmail() {
        String newEmail = modifyEmailView.getEmailFieldText();
        AccountModel account = new AccountModel();
        String message = account.updateEmail(userID, newEmail);
        if (message.equals("")) {
            EditUserController editUserController = new EditUserController(primaryStage, userID);
            editUserController.show();
        }
        else { showErrorMessage(message); }
    }

    /**
     * Show a new error message
     */
    private void showErrorMessage(String message) {
        new ErrorMessage(primaryStage, message);
    }
}
