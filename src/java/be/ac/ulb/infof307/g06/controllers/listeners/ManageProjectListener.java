package be.ac.ulb.infof307.g06.controllers.listeners;

import be.ac.ulb.infof307.g06.models.FileInfo;

import java.util.List;

public interface ManageProjectListener {
    void copyProject();
    void deleteProject();
    void changeDirectory();
    void renameProject();
    void openProject();
    void returnToMainWindow();
    List<FileInfo> getUserFileInfo();
}
