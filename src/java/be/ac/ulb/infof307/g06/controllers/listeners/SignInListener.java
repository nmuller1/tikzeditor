package be.ac.ulb.infof307.g06.controllers.listeners;

public interface SignInListener {
    void confirmButtonClicked(String username, String password);

    void signUpHyperlinkClicked();
}
