package be.ac.ulb.infof307.g06.controllers.listeners;

public interface RevertControllerListener {
    void updateConsoleCanvas(String content);
}
