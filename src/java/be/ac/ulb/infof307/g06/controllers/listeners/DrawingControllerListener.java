package be.ac.ulb.infof307.g06.controllers.listeners;

import be.ac.ulb.infof307.g06.models.drawing.DiagramShapeModel;
import javafx.scene.paint.Color;

public interface DrawingControllerListener {
    void addTikZCodeFrom(DiagramShapeModel shape);
    Integer getShapeSize();
    Color getShapeColor();
    String getShapeType();
}
