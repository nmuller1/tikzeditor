package be.ac.ulb.infof307.g06.controllers.versionning;

import be.ac.ulb.infof307.g06.controllers.listeners.RevertControllerListener;

/**
 * Class to update the console and canvas after a revert.
 */
public class RevertController {
    private RevertControllerListener listener;

    /**
     *Default constructor for RevertController class.
     */
    public RevertController(){
    }

    /**
     *Method to update the console and the canvas after a revert
     * @param content the file content after the revert construction.
     */
    public void updateConsoleCanvas(String content){
        listener.updateConsoleCanvas(content);
    }
    /**
     * @param listener to link controller to EditingController.
     */
    public void setListener(RevertControllerListener listener) {
        this.listener = listener;
    }
}
