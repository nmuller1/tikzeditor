package be.ac.ulb.infof307.g06.controllers.drawing;

import be.ac.ulb.infof307.g06.controllers.listeners.ConsoleControllerListener;
import be.ac.ulb.infof307.g06.controllers.listeners.ConsoleListener;
import be.ac.ulb.infof307.g06.models.Parser;
import be.ac.ulb.infof307.g06.views.drawing.ConsoleView;
import be.ac.ulb.infof307.g06.views.ErrorMessage;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;

import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Manage the Tikz part
 */
public class ConsoleController implements ConsoleListener {
    private final ConsoleView view;
    private final Parser parser;
    private static Pattern pattern;
    private ConsoleControllerListener listener;
    private Integer instructionPointer;
    private Stage primaryStage;

    /**
     * Used for tests.
     */
    protected ConsoleController() {
        initConstant();
        instructionPointer = 0;
        view = null;
        parser = new Parser();
    }

    public ConsoleController(String content) {
        initConstant();
        instructionPointer = 0;
        view = new ConsoleView();
        view.setListener(this);
        parser = new Parser();
        setText(content);
    }

    /**
     * Init constants
     */
    private static void initConstant() {
        String[] keywords = {"rectangle", "circle", "node"};
        String[] backslashkeywords = {"tikz", "begin", "end", "draw", "node", "fill", "color"};
        String keywordsPattern = "\\b(" + String.join("|", keywords) + ")\\b";
        String backslashKeywordsPattern = "\\\\" + "\\b(" + String.join("|", backslashkeywords) + ")\\b";
        String commentPattern = "%[^\n]*";
        String parameterPattern = "\\[.*?\\]";
        pattern = Pattern.compile("(?<KEYWORDS>" + keywordsPattern + ")" + "|(?<BACKSLASHKEYWORDS>" + backslashKeywordsPattern + ")" + "|(?<COMMENT>" + commentPattern + ")" + "|(?<PARAMETER>" + parameterPattern + ")");
    }

    /**
     * Highlight word from text with the Matcher we can use group name to
     * recognise category of item in text and apply css for example all word
     * in KEYWORD group will be applied keyword style from TikZ-keyword.css
     * @param text text that will be highlighted with the keywords.
     * @return Text highlighted.
     */
    @Override
    public StyleSpans<Collection<String>> highlightText(String text) {
        Matcher matcher = pattern.matcher(text);
        int lastEnd = 0;
        StyleSpansBuilder<Collection<String>> spansBuilder = new StyleSpansBuilder<>();
        while (matcher.find()) {
            String styleClass = matcher.group("KEYWORDS") != null ? "keywords" : matcher.group("BACKSLASHKEYWORDS") != null ? "backslash_keywords" : matcher.group("COMMENT") != null ? "comment" : matcher.group("PARAMETER") != null ? "parameter" : null; /* never happens */
            assert styleClass != null;
            spansBuilder.add(Collections.emptyList(), matcher.start() - lastEnd);
            spansBuilder.add(Collections.singleton(styleClass), matcher.end() - matcher.start());
            lastEnd = matcher.end();
        }
        spansBuilder.add(Collections.emptyList(), text.length() - lastEnd);
        return spansBuilder.create();
    }

    /**
     * Translates the text in the consoleView into drawings.
     */
    public void translate() {
        parser.goToBegin();
        parser.setText(view.getText());
        int len = ConsoleView.getCodeArea().getLength();
        int i = 0;
        String instruction = parser.getNextInstruction();
        try {
            while (!instruction.equals("0") && len > 0) {
                String[] options = new String[1];
                options[0] = parser.getLinkInInstruction(instruction);
                if (!options[0].equals("--") && !options[0].equals("rectangle") && !options[0].equals("circle")) {
                    instruction = "0";
                } else {
                    String elementary = parser.getElementaryInstruction(instruction);
                    String[] instOp = parser.getInstructionOptions(instruction);
                    Double[][] coord = parser.getCoordinates(instruction);
                    translateCodeIntoDrawing(elementary, instOp, options, coord);
                    instruction = parser.getNextInstruction();
                    len -= ConsoleView.getCodeArea().getParagraphLength(i) + 1;
                    i++;
                }
            }
        } catch (Exception e) {
            if (instruction.equals("0") && len > 0) {
                new ErrorMessage(primaryStage, "Erreur de syntaxe");
            } else {
                new ErrorMessage(primaryStage, "Erreur de codage\n" + "Error : " +e);
            }
        }
    }

    /**
     * Translate simple instructions into drawing
     * @param instruction
     * @param instructionOptions : Options about the instruction
     * @param link               : Other options
     * @param coordinates        : Coordinates
     */
    private void translateCodeIntoDrawing(String instruction, String[] instructionOptions, String[] link, Double[][] coordinates) {
        boolean filled = false;
        int size = 30;
        String type = "edge";
        Color color = Color.web(instructionOptions[0]);
        Color colorFill = color;
        if (instruction.equals("draw") || instruction.equals("fill")) {
            if (instruction.equals("fill")) {
                filled = true;
            } else {
                if (!instructionOptions[1].equals("")) {
                    size = Integer.parseInt(instructionOptions[1]);
                }
                if (instructionOptions[2].equals("1")) {
                    type = "dash";
                }
                if (instructionOptions[2].equals("2")) {
                    type = "arc";
                }
            }
        } else if (instruction.equals("filldraw")) {
            colorFill = Color.web(instructionOptions[1]);
            filled = true;
            if (!instructionOptions[2].equals(""))
                size = Integer.parseInt(instructionOptions[2]);
        }
        draw(type, color, colorFill, size, coordinates, link[0], filled);
        if (instructionPointer > 0) {
            instructionPointer--;
        }
    }

    /**
     * Draws the shape in the drawing controller
     * @param type        of the shape to draw
     * @param color       of the shape to draw
     * @param colorFill   of the shape to draw
     * @param size        to draw
     * @param coordinates of the shape to draw
     * @param link        to draw
     * @param filled      if the shape is filled
     */
    private void draw(String type, Color color, Color colorFill, Integer size, Double[][] coordinates, String link, Boolean filled) {
        if (link.equals("rectangle")) {
            if (filled) listener.fillRectangle(colorFill, size, coordinates);
            else listener.drawRectangle(type, color, size, coordinates);
        } else if (link.equals("circle")) {
            // TODO WTF coordinates a refacto
            if (filled) listener.fillCircle(colorFill, size, coordinates);
            else listener.drawCircle(color, size, coordinates);
        } else {
            for (int i = 1; i < coordinates.length; i++) {
                listener.drawShape(type, color, size, coordinates[i - 1], coordinates[i]);
            }
        }
    }

    /**
     * @return the text of the current diagram
     */
    public String getText() {
        return view.getText();
    }

    /**
     * @param text code of the diagram
     */
    public void setText(String text) {
        view.addCode(text);
    }

    /**
     * @return the view associated to the controller
     */
    public ConsoleView getView() {
        return view;
    }

    /**
     * @param listener to link controller to the view
     */
    public void setListener(ConsoleControllerListener listener) {
        this.listener = listener;
    }
}