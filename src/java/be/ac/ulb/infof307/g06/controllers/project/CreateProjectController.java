package be.ac.ulb.infof307.g06.controllers.project;

import be.ac.ulb.infof307.g06.controllers.drawing.EditingController;
import be.ac.ulb.infof307.g06.models.CreateProjectModel;
import be.ac.ulb.infof307.g06.models.Database;
import be.ac.ulb.infof307.g06.models.UserInfoModel;
import be.ac.ulb.infof307.g06.views.ErrorMessage;
import be.ac.ulb.infof307.g06.views.FileChooserView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

/**
 * Controller of the create project context
 */
public class CreateProjectController {
    private final Stage primaryStage;
    private File projectFile;
    private FileChooserView fileChooserView;
    private final UserInfoModel userInfoModel;
    private CreateProjectModel model;

    public CreateProjectController(Stage primaryStage, UserInfoModel userInfoModel) {
        this.primaryStage = primaryStage;
        this.userInfoModel = userInfoModel;
        this.model = new CreateProjectModel(userInfoModel);
    }

    /**
     * Shows the fileChooser
     */
    public void show() {
        chooseFile();
    }

    /**
     * Displays the fileChooser and then displays the edition
     */
    private void chooseFile() {
        this.fileChooserView = new FileChooserView("Save in","TEX","*.tex");
        try {
            projectFile = fileChooserView.showSaveDialogInStage(primaryStage);
            if (projectFile != null) {
                int projectId = model.createNewProject(projectFile);
                if(projectId == -1) throw new Exception();
                EditingController editingController = new EditingController(primaryStage, "", projectId, userInfoModel);
                editingController.show();
            }
        } catch (Exception e) {
            new ErrorMessage(primaryStage, "An error occurred while trying to open the project");
        }
    }
}

