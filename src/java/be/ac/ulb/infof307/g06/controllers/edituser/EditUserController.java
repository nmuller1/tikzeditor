package be.ac.ulb.infof307.g06.controllers.edituser;

import be.ac.ulb.infof307.g06.controllers.MainMenuController;
import be.ac.ulb.infof307.g06.controllers.listeners.EditUserListener;
import be.ac.ulb.infof307.g06.models.Database;
import be.ac.ulb.infof307.g06.models.UserInfoModel;
import be.ac.ulb.infof307.g06.views.ErrorMessage;
import be.ac.ulb.infof307.g06.views.edituser.EditUserView;
import javafx.stage.Stage;

/**
 * Create a new page to modify the infos of the user.
 */
public class EditUserController implements EditUserListener {
    private final EditUserView editUserView;
    private final Stage primaryStage;
    private final int userID;

    public EditUserController(Stage primaryStage, int userID) {
        this.primaryStage = primaryStage;
        this.userID = userID;
        editUserView = new EditUserView(userID);
        editUserView.setListener(this);
    }

    @Override
    /**
     * Return to the main menu.
     */
    public void exitButtonClicked() {
        Database database = new Database();
        String[] data = database.getInfoUser(userID);
        UserInfoModel userInfoModel = new UserInfoModel(userID, data[0]);
        MainMenuController mainMenuController = new MainMenuController(primaryStage, userInfoModel);
        mainMenuController.show();
    }

    @Override
    /**
     * Open the modify username page.
     */
    public void modifyUsernameButtonClicked() {
        ModifyUsernameController modifyUsernameController = new ModifyUsernameController(primaryStage, userID);
        modifyUsernameController.show();
    }

    @Override
    /**
     * Open the modify email page.
     */
    public void modifyEmailButtonClicked() {
        ModifyEmailController modifyEmailController = new ModifyEmailController(primaryStage, userID);
        modifyEmailController.show();
    }

    @Override
    /**
     * Open the modify password page.
     */
    public void modifyPasswordButtonClicked() {
        ModifyPasswordController modifyPasswordController = new ModifyPasswordController(primaryStage, userID);
        modifyPasswordController.show();
    }

    /**
     * Show the editUserView.
     */
    public void show() {
        primaryStage.setScene(editUserView.getScene());
        primaryStage.show();
    }
}
