package be.ac.ulb.infof307.g06.controllers;

import be.ac.ulb.infof307.g06.controllers.edituser.EditUserController;
import be.ac.ulb.infof307.g06.controllers.listeners.MainMenuListener;
import be.ac.ulb.infof307.g06.controllers.project.CreateProjectController;
import be.ac.ulb.infof307.g06.controllers.project.ManageProjectController;
import be.ac.ulb.infof307.g06.models.UserInfoModel;
import be.ac.ulb.infof307.g06.views.MainMenuView;
import javafx.application.Platform;
import javafx.stage.Stage;


/**
 * Control MainMenuView
 */
public class MainMenuController implements MainMenuListener {
    private final MainMenuView mainMenuView;
    private final Stage primaryStage;
    private final MenuBarController menuBarController;
    private final UserInfoModel userInfoModel;

    /**
     * Class constructor
     * @param primaryStage contains the primary stage
     */
    public MainMenuController(Stage primaryStage, UserInfoModel userInfoModel) {
        this.primaryStage = primaryStage;
        this.userInfoModel = userInfoModel;
        menuBarController = new MenuBarController(primaryStage, "MainMenu", userInfoModel);
        mainMenuView = new MainMenuView(menuBarController.getView());
        mainMenuView.setListener(this);
    }

    /**
     * Displays the fileManager. The user can then navigate his files to find
     * a place to create a new project.
     */
    @Override
    public void createProjectButtonClicked() {
        CreateProjectController createProjectController = new CreateProjectController(primaryStage, userInfoModel);
        createProjectController.show();
    }

    /**
     * Opens the project manager to rename/delete etc. projects
     */
    @Override
    public void openProjectButtonClicked() {
        ManageProjectController manageProjectController = new ManageProjectController(primaryStage, userInfoModel);
        manageProjectController.show();
    }

    /**
     * Opens the user edition menu
     */
    @Override
    public void editUserButtonClicked() {
        EditUserController editUserController = new EditUserController(primaryStage, userInfoModel.getUserID());
        editUserController.show();
    }

    /**
     * Exits the program
     */
    @Override
    public void exitButtonClicked() {
        Platform.exit();
    }

    /**
     * Displays the linked view
     */
    public void show() {
        primaryStage.setScene(mainMenuView.getScene());
        primaryStage.setTitle("Main Menu");
        primaryStage.show();
    }
}

