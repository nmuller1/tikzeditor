package be.ac.ulb.infof307.g06.controllers;

import be.ac.ulb.infof307.g06.controllers.listeners.SignUpListener;
import be.ac.ulb.infof307.g06.models.AccountModel;
import be.ac.ulb.infof307.g06.views.ErrorMessage;
import be.ac.ulb.infof307.g06.views.EulaView;
import be.ac.ulb.infof307.g06.views.SignUpView;
import javafx.stage.Stage;

import java.net.MalformedURLException;
import java.util.regex.Pattern;

public class SignUpController implements SignUpListener {
    private final SignUpView signUpView;
    private final Stage primaryStage;

    /**
     * Constructor used for tests
     */
    protected SignUpController(){
        primaryStage = null;
        signUpView = null;
    }

    /**
     * Main constructor
     * @param primaryStage associated to the view
     */
    public SignUpController(Stage primaryStage) {
        this.primaryStage = primaryStage;
        signUpView = new SignUpView();
        signUpView.setListener(this);
    }

    /**
     * called when trying to sign up
     * @param username
     * @param email
     * @param firstName
     * @param lastName
     * @param password
     * @param confirmPassword
     * @param agreed
     */
    @Override
    public void confirmButtonClicked(String username, String email, String firstName, String lastName, String password, String confirmPassword, boolean agreed) {
        attemptSignUp(username, email, firstName, lastName, password, confirmPassword, agreed);
    }

    /**
     * Displays the EULA text
     */
    @Override
    public void eulaHyperlinkClicked() {
        try{
            new EulaView(primaryStage);
        } catch (MalformedURLException e){
            new ErrorMessage(primaryStage,"An error has occurred while trying to read the EULA file");
        }
    }

    /**
     * Displays the sign in form
     */
    @Override
    public void signInHyperlinkClicked() {
        SignInController signIn = new SignInController(primaryStage);
        signIn.show();
    }

    /**
     * Displays the associated view
     */
    public void show() {
        primaryStage.setScene(signUpView.getScene());
        primaryStage.show();
    }

    /**
     * Verify if passwords are matching is if so try to create an account
     * @param username of the user
     * @param email of the user
     * @param firstName of the user
     * @param lastName of the user
     * @param password of the user
     * @param confirmPassword same as previous
     * @param agreed if agreed is checked
     */
    private void attemptSignUp(String username, String email, String firstName, String lastName, String password, String confirmPassword, boolean agreed) {
        if (!verifyIfEmpty(username, email, firstName, lastName, password, confirmPassword)) {
            if(verifyUsername(username)) {
                if (verifyPassword(password,confirmPassword)) {
                    if (verifyEmail(email)) {
                        if (agreed) {
                            if ((new AccountModel()).signUp(username, lastName, firstName, email, password) != -1) {
                                SignInController signIn = new SignInController(primaryStage);
                                signIn.show();
                            } else {
                                new ErrorMessage(primaryStage,
                                        "Email or username already exists");
                            }
                        } else {
                            new ErrorMessage(primaryStage,
                                    "Please agree to our terms of utilisation before creating your account");
                        }
                    } else {
                        new ErrorMessage(primaryStage,
                                "Please insert a valid email address!" +
                                        " Accepted domains are .be, .com, .fr, .lu, .org, .eu, .nl, .de, .edu");
                    }
                }
                else {
                    new ErrorMessage(primaryStage,
                            "Passwords are not matching and must be at least 8 characters long");
                }
            }
            else {
                new ErrorMessage(primaryStage,
                        "Username should only contain lower cases and no special characters");
            }
        }
        else {
            new ErrorMessage(primaryStage, "Please fill each field");
        }
    }

    /**
     * Verify if any field were left empty
     * @param username of the user
     * @param email of the user
     * @param firstName of the user
     * @param lastName of the user
     * @param password of the user
     * @param confirmPassword the same than the previous
     * @return true if at least one field is empty else return false.
     */
    protected boolean verifyIfEmpty(String username, String email, String firstName, String lastName, String password, String confirmPassword) {
        return username.isEmpty() || email.isEmpty() || firstName.isEmpty() || lastName.isEmpty() || password.isEmpty() || confirmPassword.isEmpty();
    }

    /**
     * Verify if the mail is in a correct format
     * @param email
     * @return true if email is a valid one. False otherwise
     */
    protected boolean verifyEmail(String email) {
        // https://www.geeksforgeeks.org/check-email-address-valid-not-java/
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null) {
            return false;
        } return pat.matcher(email).matches();
    }

    /**
     * Check if username contains only lower cases (at least one letter) and no special characters
     * @param username
     * @return true is username is valid, else otherwise
     */
    protected boolean verifyUsername(String username) {
        return username.matches("[a-z]+");
    }

    /**
     * Check if passwords are the same and are at least 8 characters long
     * @param password
     * @param confirmPassword
     * @return true if passwords are valid, else otherwise
     */
    protected boolean verifyPassword(String password, String confirmPassword) {
        return password.equals(confirmPassword) && password.length() >= 8;
    }
}
