package be.ac.ulb.infof307.g06.controllers.project;

import be.ac.ulb.infof307.g06.controllers.drawing.EditingController;
import be.ac.ulb.infof307.g06.controllers.MainMenuController;
import be.ac.ulb.infof307.g06.controllers.MenuBarController;
import be.ac.ulb.infof307.g06.controllers.listeners.ManageProjectListener;
import be.ac.ulb.infof307.g06.models.ManageProjectModel;
import be.ac.ulb.infof307.g06.models.FileInfo;
import be.ac.ulb.infof307.g06.models.UserInfoModel;
import be.ac.ulb.infof307.g06.views.ErrorMessage;
import be.ac.ulb.infof307.g06.views.project.ManageProjectView;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * Link the ManageProjectView and the ManageProjectModel.
 * Verify that the right number of info is selected in the GUI.
 */
public class ManageProjectController implements ManageProjectListener {
    private final ManageProjectView manageProjectView;
    private final ManageProjectModel model;
    private final Stage primaryStage;
    private final UserInfoModel userInfoModel;

    public ManageProjectController(Stage primaryStage, UserInfoModel userInfoModel) {
        this.primaryStage = primaryStage;
        this.userInfoModel = userInfoModel;
        model = new ManageProjectModel(userInfoModel);
        MenuBarController menuBarController = new MenuBarController(primaryStage, "ManageProject", userInfoModel);
        manageProjectView = new ManageProjectView(menuBarController.getView());
        manageProjectView.setListener(this);
        manageProjectView.setFileTable();
    }

    /**
     * Connect to copy in model.
     * Create a new file that is the same as the copied one.
     * The new name is "name_copy".
     */
    @Override
    public void copyProject() {
        if (manageProjectView.getCurrentSelectedIndex() != -1) {
            try{
                for (FileInfo fileInfo : manageProjectView.getSelectedItems()) {
                    model.copyProject(fileInfo);
                }
            } catch (IOException e){
                new ErrorMessage(primaryStage,"An error has occurred during the file creation process.\n" +
                        "Error : " + e);
            }
        }
        manageProjectView.setFileTable();
    }

    /**
     * Connect to delete in model.
     * Delete the file.
     */
    @Override
    public void deleteProject() {
        if (manageProjectView.getCurrentSelectedIndex() != -1) {
            for (FileInfo fileInfo : manageProjectView.getSelectedItems()) {
                model.deleteProject(fileInfo);
            }
        }
        manageProjectView.setFileTable();
    }

    /**
     * Connect to newDir in model.
     * Move the file in a new directory.
     */
    @Override
    public void changeDirectory() {
        if (manageProjectView.getCurrentSelectedIndex() != -1) {
            for (FileInfo fileInfo : manageProjectView.getSelectedItems()) {
                DirectoryChooser directoryChooser = new DirectoryChooser();
                File newDirectory = directoryChooser.showDialog(this.primaryStage);
                try {
                    model.changeDirectory(fileInfo,newDirectory.getAbsolutePath());
                }catch (IOException e){
                    new ErrorMessage(primaryStage, "Could not move the file to another directory.\n error meesage :"+e);
                }

            }
        }
    }

    /**
     * Connect to rename in model.
     * Change the name of the file.
     */
    @Override
    public void renameProject() {
        if (manageProjectView.getCurrentSelectedIndex() != -1) {
            if (manageProjectView.getSelectedItems().size() == 1) {
                FileInfo fileInfo = manageProjectView.getSelectedItems().get(0);
                RenameProjectController renameProjectController = new RenameProjectController(primaryStage, model, fileInfo);
                renameProjectController.show();
            } else {
                manageProjectView.showAlertMoreThanOneFile();
            }
        }
        manageProjectView.setFileTable();
    }

    /**
     * Connect to edit in model.
     * Open the drawing view and start editing the file.
     */
    @Override
    public void openProject() {
        if (manageProjectView.getCurrentSelectedIndex() != -1) {
            if (manageProjectView.getSelectedItems().size() == 1) {
                try{
                    FileInfo fileInfo = manageProjectView.getSelectedItems().get(0);
                    String content = model.editProject(fileInfo);
                    EditingController editingController = new EditingController(primaryStage, content, fileInfo.getProjectId(), userInfoModel);
                    editingController.show();
                } catch (IOException e){
                    new ErrorMessage(primaryStage,"An error has occurred while trying to read the file\n"+
                            "Error : " +e);
                }
            } else {
                manageProjectView.showAlertMoreThanOneFile();
            }
        }
    }

    /**
     * Close the project management window, and open the main menu window.
     */
    @Override
    public void returnToMainWindow() {
        //todo replace userid
        MainMenuController mainMenuController = new MainMenuController(primaryStage, userInfoModel);
        mainMenuController.show();
    }

    /**
     * @return the user infos
     */
    @Override
    public List<FileInfo> getUserFileInfo() {
        return model.getUserFileInfo();
    }

    /**
     * Displays the linked view
     */
    public void show() {
        primaryStage.setScene(manageProjectView.getScene());
        primaryStage.show();
    }
}
