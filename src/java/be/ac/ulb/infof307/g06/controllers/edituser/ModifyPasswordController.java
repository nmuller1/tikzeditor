package be.ac.ulb.infof307.g06.controllers.edituser;

import be.ac.ulb.infof307.g06.controllers.listeners.ModifyUserListener;
import be.ac.ulb.infof307.g06.models.AccountModel;
import be.ac.ulb.infof307.g06.models.Database;
import be.ac.ulb.infof307.g06.views.ErrorMessage;
import be.ac.ulb.infof307.g06.views.edituser.ModifyPasswordView;
import javafx.stage.Stage;

/**
 * Create a new page to modify the password of the user.
 * Verify the infos before modifying.
 */
public class ModifyPasswordController implements ModifyUserListener {
    private final ModifyPasswordView modifyPasswordView;
    private final Stage primaryStage;
    private final int userID;

    public ModifyPasswordController(Stage primaryStage, int userID) {
        this.primaryStage = primaryStage;
        this.userID = userID;
        modifyPasswordView = new ModifyPasswordView();
        modifyPasswordView.setListener(this);
    }

    @Override
    /**
     * Try to modify the password.
     */ public void confirmButtonClicked() {
        updatingPassword();
    }

    @Override
    /**
     * Return to the edit user page.
     */ public void cancelButtonClicked() {
        EditUserController editUserController = new EditUserController(primaryStage, userID);
        editUserController.show();
    }

    /**
     * Show the modifyEmailView.
     */
    public void show() {
        primaryStage.setScene(modifyPasswordView.getScene());
        primaryStage.show();
    }

    /**
     * Verify if the old password is the same and if the new one is valid, then modify it.
     * Return to the edit user page.
     */
    private void updatingPassword() {
        String oldPassword = modifyPasswordView.getOldPasswordFieldText();
        String newPasswordOne = modifyPasswordView.getNewPasswordFieldOneText();
        String newPasswordTwo = modifyPasswordView.getNewPasswordFieldTwoText();
        AccountModel account = new AccountModel();

        String message = account.updatePassword(userID, oldPassword, newPasswordOne, newPasswordTwo);
        if (message.equals("")) {
            EditUserController editUserController = new EditUserController(primaryStage, userID);
            editUserController.show();
        }
        else { showErrorMessage(message); }
    }

    /**
     * Show a new error message
     */
    private void showErrorMessage(String message) { new ErrorMessage(primaryStage, message); }
}
