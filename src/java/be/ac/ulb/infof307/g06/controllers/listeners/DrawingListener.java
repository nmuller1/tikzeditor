package be.ac.ulb.infof307.g06.controllers.listeners;

import be.ac.ulb.infof307.g06.models.drawing.DiagramShapeModel;
import javafx.scene.paint.Color;

public interface DrawingListener {
    void addTikZCodeFrom(DiagramShapeModel shape);
    DiagramShapeModel createShape(String lineType, Color shapeColor, Integer size, Double[][] coordinates);
    DiagramShapeModel createShape(String lineType, Color shapeColor, Integer size, Double[] coordinates);
    DiagramShapeModel createShape(String lineType, Color shapeColor, Integer size, Double[] firstCoordinates, Double[] secondCoordinates);
    DiagramShapeModel createShape(Color shapeColor, Integer size, Double[] coordinates);
    String getShapeType();
    Color getShapeColor();
    Integer getShapeSize();
}
