package be.ac.ulb.infof307.g06.controllers.listeners;

import org.fxmisc.richtext.model.StyleSpans;

import java.util.Collection;

public interface ConsoleListener {
    StyleSpans<? extends Collection<String>> highlightText(String newText);
}
