package be.ac.ulb.infof307.g06.controllers;

import be.ac.ulb.infof307.g06.controllers.listeners.SignInListener;
import be.ac.ulb.infof307.g06.models.AccountModel;
import be.ac.ulb.infof307.g06.models.UserInfoModel;
import be.ac.ulb.infof307.g06.views.ErrorMessage;
import be.ac.ulb.infof307.g06.views.SignInView;
import javafx.stage.Stage;

/**
 * Control SignInView
 */
public class SignInController implements SignInListener {
    private SignInView view;
    private Stage primaryStage;
    private AccountModel model;

    /**
     * Class constructor
     * @param primaryStage contains the primary stage
     */
    public SignInController(Stage primaryStage){
        this.primaryStage = primaryStage;
        this.model = new AccountModel();
        this.view = new SignInView();
        view.setListener(this);
    }

    public void show() {
        primaryStage.setScene(view.getScene());
        primaryStage.show();
    }

    /**
     * @param username of the user
     * @param password of the user
     */
    @Override
    public void confirmButtonClicked(String username, String password){
        attemptSignIn(username,password);
    }

    @Override
    public void signUpHyperlinkClicked(){
        SignUpController signUp = new SignUpController(primaryStage);
        signUp.show();
    }

    /**
     * Verify if username and password match with an existing user and connects
     * @param username of the user
     * @param password of the user
     */
    private void attemptSignIn(String username, String password) {
        if (!verifyIfEmpty(username,password)) {
            int userId = model.signIn(username,password);
            if (userId != -1) {
                UserInfoModel userInfoModel = new UserInfoModel(userId,username);
                MainMenuController mainMenuController = new MainMenuController(primaryStage, userInfoModel);
                mainMenuController.show();
            }
            else {
                new ErrorMessage(primaryStage, "Invalid username or password");

            }
        }
        else {
            new ErrorMessage(primaryStage, "Please complete each field");
        }
    }

    /**
     * Verify if any field were left empty
     * @param username of the user
     * @param password of the user
     * @return
     */
    protected boolean verifyIfEmpty(String username, String password) {
        return username.isEmpty() || password.isEmpty();
    }
}
