package be.ac.ulb.infof307.g06.controllers.listeners;

public interface MenuBarEditingControllerListener {
    void saveProject();
    void importProjectButtonClicked();
    void exportProjectButtonClicked();
}
