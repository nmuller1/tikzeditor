package be.ac.ulb.infof307.g06.controllers.listeners;

import be.ac.ulb.infof307.g06.models.FileInfo;

public interface RenameProjectListener {
    void renameFile(String newName, FileInfo fileInfo);
    void closeWindow();
}
