package be.ac.ulb.infof307.g06.controllers.edituser;

import be.ac.ulb.infof307.g06.controllers.listeners.ModifyUserListener;
import be.ac.ulb.infof307.g06.models.AccountModel;
import be.ac.ulb.infof307.g06.models.Database;
import be.ac.ulb.infof307.g06.views.ErrorMessage;
import be.ac.ulb.infof307.g06.views.edituser.ModifyUsernameView;
import javafx.stage.Stage;

/**
 * Create a new page to modify the username of the user.
 * Verify the infos before modifying.
 */
public class ModifyUsernameController implements ModifyUserListener {
    private final ModifyUsernameView modifyUsernameView;
    private final Stage primaryStage;
    private final int userID;

    public ModifyUsernameController(Stage primaryStage, int userID) {
        this.primaryStage = primaryStage;
        this.userID = userID;
        modifyUsernameView = new ModifyUsernameView(userID);
        modifyUsernameView.setListener(this);
    }

    /**
     * Try to modify the username.
     */
    @Override
    public void confirmButtonClicked() {
        updatingUsername();
    }

    /**
     * Return to the edit user page.
     */
    @Override
    public void cancelButtonClicked() {
        EditUserController editUserController = new EditUserController(primaryStage, userID);
        editUserController.show();
    }

    /**
     * Show the modifyUsernameView.
     */
    public void show() {
        primaryStage.setScene(modifyUsernameView.getScene());
        primaryStage.show();
    }

    /**
     * Verify if the username is valid and available, then modify it.
     * Return to the edit user page.
     */
    private void updatingUsername() {
        String newUsername = modifyUsernameView.getUsernameFieldText();
        AccountModel account = new AccountModel();
        String message = account.updateUsername(userID, newUsername);
        if (message.equals("")) {
            EditUserController editUserController = new EditUserController(primaryStage, userID);
            editUserController.show();
        }
        else { showErrorMessage(message); }
    }

    /**
     * Show a new error message
     */
    private void showErrorMessage(String message) {
        new ErrorMessage(primaryStage, message);
    }
}
