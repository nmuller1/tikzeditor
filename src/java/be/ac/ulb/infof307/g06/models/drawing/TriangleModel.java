package be.ac.ulb.infof307.g06.models.drawing;

import be.ac.ulb.infof307.g06.models.drawing.DiagramShapeModel;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.stream.Stream;

/**
 * Manage the triangle model.
 * Create a triangle with his 3 nodes.
 */
public class TriangleModel extends DiagramShapeModel {
    private int length;
    private Double[] firstNodeCoordinates;
    private Double[] secondNodeCoordinates;
    private Double[] thirdNodeCoordinates;

    public TriangleModel(String type, Color color, Integer size, Double[] coordinates) {
        super(type, color, size, coordinates, 3);
        determineNodes();
    }

    /**
     * Draws the triangle.
     * @param graphicsContext
     */
    @Override
    public void draw(GraphicsContext graphicsContext) {
        graphicsContext.setStroke(color);
        graphicsContext.setFill(color);
        Double[] xPoints = getXVertices();
        Double[] yPoints = getYVertices();
        graphicsContext.fillPolygon(Stream.of(xPoints).mapToDouble(Double::doubleValue).toArray(), Stream.of(yPoints).mapToDouble(Double::doubleValue).toArray(), edgeNumber);
    }

    /**
     * @return table of 6, every 2 elements, are the x and y coordinates of the 3 nodes.
     */
    @Override
    public Double[] getCoordinates() {
        return new Double[]{firstNodeCoordinates[0], firstNodeCoordinates[1], secondNodeCoordinates[0], secondNodeCoordinates[1], thirdNodeCoordinates[0], thirdNodeCoordinates[1]};
    }

    /**
     * @return Get the TikZ Code, with its sides and filling color set.
     */
    @Override
    public String getTikZCode() {
        Double[] coordinates = getCoordinates();
        String tikzCommand = String.format("\\draw[color=%s,fill=%s] (%d,%d) -- (%d,%d) -- (%d,%d) -- cycle ;", color, color, coordinates[0].intValue(), coordinates[1].intValue(), coordinates[2].intValue(), coordinates[3].intValue(), coordinates[4].intValue(), coordinates[5].intValue());
        return tikzCommand;
    }

    /**
     * Calculates the x and y coordinates of the 3 nodes of the triangle
     * using its length (equilateral triangle) and its center
     */
    public void determineNodes() {
        firstNodeCoordinates = new Double[]{firstCoordinates[0], firstCoordinates[1]};
        secondNodeCoordinates = new Double[]{firstCoordinates[0] + size, firstCoordinates[1]};
        thirdNodeCoordinates = new Double[]{firstCoordinates[0] + size / 2.0, firstCoordinates[1] + size / 2.0};
    }

    /**
     * @return the x coordinates of the 3 nodes.
     */
    public Double[] getXVertices() {
        return new Double[]{firstNodeCoordinates[0], secondNodeCoordinates[0], thirdNodeCoordinates[0]};
    }

    /**
     * @return the y coordinates of the 3 nodes.
     */
    public Double[] getYVertices() {
        return new Double[]{firstNodeCoordinates[1], secondNodeCoordinates[1], thirdNodeCoordinates[1]};
    }
}
