package be.ac.ulb.infof307.g06.models;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Enables to connect to sqlite database
 */
public class Database {
    private static final String PATH = "database.db";
    private static Connection connection;

    public Database() {
        connection = null;
    }

    public void saveProject(int projectID, String text) {
        Database database = new Database();
        database.updateModificationDate(projectID);
        String[] fileInfo;
        fileInfo = database.getProjectData(projectID);
        File fileOld = new File(fileInfo[1]);
        fileOld.delete();
        File fileNew = new File(fileInfo[1]);
        try {
            FileWriter f2 = new FileWriter(fileNew, false);
            f2.write(text);
            f2.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * inserts a new project in the database
     * @param userID     owner's id of the file
     * @param name       name of the file
     * @param pathToFile absolute path to the file in user's computer
     * @return id of the inserted project
     */
    public int insertProject(int userID, String name, String pathToFile) {
        try {
            connectToDataBase();
            String sql = "INSERT INTO Projects(USER_ID, NAME, PATH_TO_FILE, CREATION_DATE, MODIFICATION_DATE) VALUES(?,?,?, DATETIME('NOW'),DATETIME('NOW'))";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, userID);
            statement.setString(2, name);
            statement.setString(3, pathToFile);
            statement.executeUpdate();

            // provides the id of the new project
            Statement stmt = connection.createStatement();
            ResultSet data = stmt.executeQuery("SELECT ID FROM Projects ORDER BY ID DESC LIMIT 1");
            int ret = data.getInt("ID");

            disconnectFromDataBase();
            return ret;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return -1;
        }
    }

    /**
     * provides all projects ids of the user
     * @param userID user's ID
     * @return list of all project ids of the user
     */
    public List<Integer> getProjectsOfUser(int userID) {
        String sql = "SELECT ID FROM Projects WHERE USER_ID = ?";
        List<Integer> identifier = new ArrayList<>();
        identifier.add(userID);
        return getManyIntegerQuery(sql, "ID", identifier);
    }

    /**
     * provides all data of a specified project
     * @param id id of the project
     * @return name, path_to_file, creation_date, modification_date
     */
    public String[] getProjectData(int id) {
        String sql = "SELECT * FROM Projects WHERE ID = ?";
        String[] dataLabels = {"NAME","PATH_TO_FILE","CREATION_DATE","MODIFICATION_DATE"};
        List<Integer> identifier = new ArrayList<>();
        identifier.add(id);
        return getManyStringQuery(sql,dataLabels,identifier);
    }

    /**
     * rename a project
     * @param id      id of the project
     * @param newName new name of the project
     */
    public void renameProject(int id, String newName) {
        String sql = "UPDATE Projects SET NAME = ? WHERE ID = ?";
        updateStringValue(sql,newName,id);
    }

    /**
     * update project modification date to current date
     * @param id id of the project
     */
    public void updateModificationDate(int id) {
        String sql = "UPDATE Projects SET MODIFICATION_DATE = DATETIME('NOW') WHERE ID = ?";

        try {
            connectToDataBase();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.executeUpdate();

            disconnectFromDataBase();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * modify the path to the project
     * @param id      id of the project
     * @param newPath new path of the project
     */
    public void updatePath(int id, String newPath) {
        String sql = "UPDATE Projects SET PATH_TO_FILE = ? WHERE ID = ?";
        updateStringValue(sql,newPath,id);
    }

    /**
     * delete a project
     * @param id id of the project
     */
    public void deleteProject(int id) {
        String sql = "DELETE FROM Projects WHERE ID = ?";
        try {
            connectToDataBase();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.executeUpdate();

            disconnectFromDataBase();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Check if path exists in database
     * @param path of the file
     * @return true if project exists
     */
    public boolean checkProjectExist(String path) {
        String sql = "SELECT * FROM Projects WHERE PATH_TO_FILE=?";
        return getBooleanQuery(sql,path);
    }

    /**
     *  Insert a new branch into the database
     * @param projectID id of the project
     * @param name of the branch
     * @return branchID or -1
     */
    public int insertBranch(int projectID, String name) {
        String sql = "INSERT INTO Branchs(PROJECT_ID,BRANCH_ID,NAME,CREATION_DATE,STATUS) VALUES(?,?,?,DATETIME('NOW'),?)";
        int branchID = getLastBranchID(projectID);
        branchID +=1;
        try {
            connectToDataBase();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, projectID);
            statement.setInt(2, branchID);
            statement.setString(3, name);
            statement.setInt(4, 1);
            statement.executeUpdate();
            disconnectFromDataBase();
            return branchID;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    /**
     * @param projectID id of the project
     * @return get last branch id
     */
    public int getLastBranchID(int projectID) {
        String sql = "SELECT BRANCH_ID FROM Branchs WHERE PROJECT_ID = ? ORDER BY BRANCH_ID  DESC LIMIT 1;";
        List<Integer> identifier = new ArrayList<>();
        identifier.add(projectID);
        return getSingleIntQuery(sql,"BRANCH_ID",identifier);
    }

    /**
     * updates branch status to indicates if a branch is deleted or still active
     * @param projectID if of the project
     * @param branchID if of the branch
     * @param branchStatus 0 if "deleted", 1 if still active
     */
    public void updateBranchStatus(int projectID, int branchID, int branchStatus) {
        String sql = "UPDATE Branchs SET STATUS = ? WHERE PROJECT_ID = ? AND BRANCH_ID = ?";
        try {
            connectToDataBase();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, branchStatus);
            statement.setInt(2, projectID);
            statement.setInt(3, branchID);
            statement.executeUpdate();
            disconnectFromDataBase();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * gets list of branches of a given project
     * @param projectID id of the project
     * @return list of branches' ID
     */
    public List<Integer> getBranches(int projectID) {
        String sql = "SELECT BRANCH_ID FROM Branchs WHERE PROJECT_ID = ?";
        List<Integer> identifier = new ArrayList<>();
        identifier.add(projectID);
        return getManyIntegerQuery(sql, "BRANCH_ID", identifier);
    }


    /**
     * gets list of active branches of a given project
     * @param projectID id of the project
     * @return list of active branches' ID
     */
    public List<Integer> getActiveBranches(int projectID) {
        String sql = "SELECT BRANCH_ID FROM Branchs WHERE PROJECT_ID = ? AND STATUS = '1'";
        List<Integer> identifier = new ArrayList<>();
        identifier.add(projectID);
        return getManyIntegerQuery(sql, "BRANCH_ID", identifier);
    }

    /**
     * provides all data of a specified branch of project
     * @param projectID id of the project
     * @param branchID id of the branch
     * @return projectID, branchID, name, creation date, status (0 if deleted, 1 if still active)
     */
    public String[] getBranchData(int projectID, int branchID) {
        String sql = "SELECT * FROM Branchs WHERE PROJECT_ID = ? AND BRANCH_ID = ?";
        String[] dataLabels = {"PROJECT_ID","BRANCH_ID","NAME","CREATION_DATE","STATUS"};
        List<Integer> identifiers = new ArrayList<>();
        identifiers.add(projectID);
        identifiers.add(branchID);
        return getManyStringQuery(sql,dataLabels,identifiers);
    }

    /**
     * inserts a new commit in the database
     * @param projectID id of the project
     * @param branchID id of the branch
     * @param message commit's message
     * @return id of the inserted commit
     */
    public int insertCommit(int projectID, int branchID, String message) {
        String sql = "INSERT INTO Commits(PROJECT_ID,BRANCH_ID,COMMIT_ID,MESSAGE,CREATION_DATE) VALUES(?,?,?,?,DATETIME('NOW'))";
        int commitID = getLastCommitID(projectID,branchID);
        commitID +=1;
        try {
            connectToDataBase();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, projectID);
            statement.setInt(2, branchID);
            statement.setInt(3, commitID);
            statement.setString(4, message);
            statement.executeUpdate();
            disconnectFromDataBase();
            return commitID;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return -1;

    }

    public int getLastCommitID(int projectID, int branchID) {
        String sql = "SELECT COMMIT_ID FROM Commits WHERE PROJECT_ID = ? AND BRANCH_ID = ? ORDER BY COMMIT_ID DESC LIMIT 1;";
        List<Integer> identifiers = new ArrayList<>();
        identifiers.add(projectID);
        identifiers.add(branchID);
        return getSingleIntQuery(sql,"COMMIT_ID",identifiers);
    }

    /**
     * provides all commits ids of a branch
     * @param projectID id of the project
     * @param branchID if of the branch
     * @return list of all commits of a given branch
     */
    public List<Integer> getCommits(int projectID, Integer branchID) {
        String sql = "SELECT COMMIT_ID FROM Commits WHERE PROJECT_ID = ? AND BRANCH_ID = ? ";
        List<Integer> identifiers = new ArrayList<>();
        identifiers.add(projectID);
        identifiers.add(branchID);
        return getManyIntegerQuery(sql, "COMMIT_ID", identifiers);
    }

    /**
     * provides all data of a specified branch of project
     * @param projectID id of the project
     * @param branchID if of the branch
     * @param commitID if of the commut
     * @return projectID, branchID, commitID, message of the commit, creation date
     */
    public String[] getCommitData(int projectID, int branchID, int commitID) {
        String sql = "SELECT * FROM Commits WHERE PROJECT_ID = ? AND BRANCH_ID = ? AND COMMIT_ID = ?";
        String[] dataLabels = {"PROJECT_ID","BRANCH_ID", "COMMIT_ID","MESSAGE","CREATION_DATE"};
        List<Integer> identifiers = new ArrayList<>();
        identifiers.add(projectID);
        identifiers.add(branchID);
        identifiers.add(commitID);
        return getManyStringQuery(sql,dataLabels,identifiers);
    }

    /**
     * Insert a new user into the database
     * @param username
     * @param lastName
     * @param firstName
     * @param email
     * @param password
     * @return id of the inserted user or -1
     */
    public int createUser(String username, String lastName, String firstName, String email, String password) {
        String sql = "INSERT INTO Users(USER,NAME,GIVEN_NAME,EMAIL,PASSWORD) VALUES(?,?,?,?,?)";

        try {
            connectToDataBase();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, username);
            statement.setString(2, lastName);
            statement.setString(3, firstName);
            statement.setString(4, email);
            statement.setString(5, password);
            statement.executeUpdate();

            Statement stmt = connection.createStatement();
            ResultSet data = stmt.executeQuery("SELECT ID FROM Users ORDER BY ID DESC LIMIT 1");
            int ret = data.getInt("ID");

            disconnectFromDataBase();
            return ret;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    public int getUserID(String username) {
        String sql = "SELECT ID FROM Users WHERE USER=?";

        try {
            connectToDataBase();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, username);
            ResultSet data = statement.executeQuery();
            int ret = data.getInt("ID");

            disconnectFromDataBase();
            return ret;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    /**
     * Return all the user's informations
     * @param id user's id
     * @return username, name, given name, email, password
     */
    public String[] getInfoUser(int id) {
        String sql = "SELECT USER, NAME, GIVEN_NAME, EMAIL, PASSWORD FROM Users WHERE ID=?";
        String[] dataLabels = {"USER","NAME","GIVEN_NAME","EMAIL","PASSWORD"};
        List<Integer> idArray = new ArrayList<>();
        idArray.add(id);
        return getManyStringQuery(sql,dataLabels,idArray);
    }

    /**
     * Check if a username is available
     * @param username
     * @return true if username is available == not already taken
     */
    public boolean isUsernameAvailable(String username) {
        String sql = "SELECT * FROM Users WHERE USER=?";
        return !getBooleanQuery(sql,username);
    }

    /**
     * Check if an email is available
     * @param email
     * @return true if email is available == not already taken
     */
    public boolean isEmailAvailable(String email) {
        String sql = "SELECT * FROM Users WHERE EMAIL=?";
        return !getBooleanQuery(sql,email);
    }

    /**
     * Modify the user's email
     * @param userId
     * @param newEmail
     */
    public void modifyEmail(int userId, String newEmail) {
        String sql = "UPDATE Users SET EMAIL = ? WHERE ID = ?";
        updateStringValue(sql,newEmail,userId);
    }

    /**
     * Modify the user's password
     * @param userId
     * @param newPassword
     */
    public void modifyPassword(int userId, String newPassword) {
        String sql = "UPDATE Users SET PASSWORD = ? WHERE ID = ?";
        updateStringValue(sql,newPassword,userId);
    }

    /**
     * Modify the user's username
     * @param userId
     * @param newUsername
     */
    public void modifyUsername(int userId, String newUsername) {
        String sql = "UPDATE Users SET USER = ? WHERE ID = ?";
        updateStringValue(sql,newUsername,userId);
    }

    public void erase() {
        File file = new File(PATH);
        file.delete();
    }

    /**
     * update row's single string attribute
     * @param sql update instruction
     * @param newVal new string value to set
     * @param identifier identifier of the row to update
     */
    private void updateStringValue(String sql, String newVal, int identifier) {
        try {
            connectToDataBase();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, newVal);
            statement.setInt(2, identifier);
            statement.executeUpdate();
            disconnectFromDataBase();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * checks if val value exists in the given table
     * @param sql query instruction
     * @param val the value of interest
     * @return true if value exists
     */
    private boolean getBooleanQuery(String sql, String val) {
        try {
            connectToDataBase();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, val);
            ResultSet value = statement.executeQuery();
            boolean ret = value.next();
            disconnectFromDataBase();
            return ret;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    /**
     * gets single int sql query result
     * @param sql query instruction
     * @param dataLabel field names of the table you want to select data from
     * @param identifiers list of identifiers of the rows to select
     * @return single int data
     */
    private int getSingleIntQuery(String sql, String dataLabel, List<Integer> identifiers) {
        try {
            connectToDataBase();
            PreparedStatement stmt = connection.prepareStatement(sql);
            for (int i = 0; i < identifiers.size(); i++) {
                stmt.setInt(i+1, identifiers.get(i));
            }
            ResultSet data = stmt.executeQuery();
            int val = data.getInt(dataLabel);
            disconnectFromDataBase();
            return val;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    /**
     * gets sql query result as a tab of strings
     * @param sql query instruction
     * @param dataLabels fields names of the table you want to select data from
     * @param identifiers list of identifiers of the rows to select
     * @return string tab data
     */
    private String[] getManyStringQuery(String sql, String[] dataLabels, List<Integer> identifiers) {
        int nbData = dataLabels.length;
        String[] tab = new String[nbData];
        try {
            connectToDataBase();
            PreparedStatement statement = connection.prepareStatement(sql);
            for (int i = 0; i < identifiers.size(); i++) {
                statement.setInt(i+1, identifiers.get(i));
            }
            ResultSet data = statement.executeQuery();
            while (data.next()) {
                for (int i = 0; i < nbData; i++) {
                    tab[i] = data.getString(dataLabels[i]);
                }
            }
            disconnectFromDataBase();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return tab;
    }

    /**
     * gets sql query result as a list of integer value
     * @param sql query instruction
     * @param dataLabel field name of the table you want to select data from
     * @param identifiers list of identifiers of the rows to select
     * @return list of integer data
     */
    private List<Integer> getManyIntegerQuery(String sql, String dataLabel, List<Integer> identifiers){
        List<Integer> data = new ArrayList<>();
        try {
            connectToDataBase();
            PreparedStatement statement = connection.prepareStatement(sql);
            for (int i = 0; i < identifiers.size(); i++) {
                statement.setInt(i+1, identifiers.get(i));
            }
            ResultSet results = statement.executeQuery();

            while (results.next()) {
                data.add(results.getInt(dataLabel));
            }
            disconnectFromDataBase();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return data;
    }

    /**
     * Establish connexion to database and set up connection attribute
     */
    private static void connectToDataBase() {
        if (connection == null) {
            try {
                Class.forName("org.sqlite.JDBC"); //loads the driver
                String url = "jdbc:sqlite:" + Database.PATH;
                connection = DriverManager.getConnection(url);
                Statement stmt = connection.createStatement();
                String sql = "PRAGMA foreign_keys=ON";
                stmt.execute(sql);
                createProjectsTable();
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
            }
        }
    }

    /**
     * Disconnect from the database and reset connection attribute
     */
    private void disconnectFromDataBase() {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * Creates SQLite tables if not already existing
     */
    private static void createProjectsTable() {

        try {
            Statement stmt = connection.createStatement();
            String usersTable = "CREATE TABLE IF NOT EXISTS Users ("
                                    + "ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                                    + "USER VARCHAR(100) NOT NULL,"
                                    + "NAME VARCHAR(100) NOT NULL,"
                                    + "GIVEN_NAME VARCHAR(100) NOT NULL,"
                                    + "EMAIL VARCHAR(100) NOT NULL,"
                                    + "PASSWORD VARCHAR(100) NOT NULL);";
            stmt.execute(usersTable);
            String projectsTable = "CREATE TABLE IF NOT EXISTS Projects ("
                                    + "ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                                    + "USER_ID INTEGER NOT NULL,"
                                    + "NAME VARCHAR(100) NOT NULL,"
                                    + "PATH_TO_FILE VARCHAR(100) NOT NULL,"
                                    + "CREATION_DATE DATETIME NOT NULL,"
                                    + "MODIFICATION_DATE DATETIME NOT NULL)";
            stmt.execute(projectsTable);
            String branchsTable = "CREATE TABLE IF NOT EXISTS Branchs ("
                                    + "PROJECT_ID INTEGER NOT NULL,"
                                    + "BRANCH_ID INTEGER NOT NULL,"
                                    + "NAME VARCHAR(100) NOT NULL,"
                                    + "CREATION_DATE DATETIME NOT NULL,"
                                    + "STATUS INTEGER NOT NULL,"
                                    + "PRIMARY KEY(PROJECT_ID, BRANCH_ID),"
                                    + "FOREIGN KEY (PROJECT_ID) REFERENCES Projects (ID) ON DELETE CASCADE)";
            stmt.execute(branchsTable);
            String commitsTable = "CREATE TABLE IF NOT EXISTS Commits ("
                                    + "PROJECT_ID INTEGER NOT NULL,"
                                    + "BRANCH_ID INTEGER NOT NULL,"
                                    + "COMMIT_ID INTEGER NOT NULL,"
                                    + "MESSAGE VARCHAR(100) NOT NULL,"
                                    + "CREATION_DATE DATETIME NOT NULL,"
                                    + "PRIMARY KEY(PROJECT_ID, BRANCH_ID, COMMIT_ID),"
                                    + "FOREIGN KEY (PROJECT_ID, BRANCH_ID) REFERENCES Branchs (PROJECT_ID,BRANCH_ID) ON DELETE CASCADE)";
            stmt.execute(commitsTable);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}