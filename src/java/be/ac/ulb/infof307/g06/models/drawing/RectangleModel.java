package be.ac.ulb.infof307.g06.models.drawing;

import be.ac.ulb.infof307.g06.models.drawing.DiagramShapeModel;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * Manage the rectangle model.
 * Create a rectangle with 2 of its opposites nodes.
 */
public class RectangleModel extends DiagramShapeModel {
    private Double[] firstNodeCoordinates;
    private Double[] secondNodeCoordinates;

    public RectangleModel(String type, Color color, Integer size, Double[] coordinates) {
        super(type, color, size, coordinates, 4);
        determineNodes();
    }

    public RectangleModel(String type, Color color, Integer size, Double[][] coordinates) {
        super(type, color, size, coordinates[0], coordinates[1], 4);
        firstNodeCoordinates = coordinates[0];
        secondNodeCoordinates = coordinates[1];
    }

    /**
     * Draws the rectangle
     */
    @Override
    public void draw(GraphicsContext graphicsContext) {
        Double sizeH = firstNodeCoordinates[0] - secondNodeCoordinates[0];
        Double sizeV = firstNodeCoordinates[1] - secondNodeCoordinates[1];
        Double firstcoord = firstNodeCoordinates[0];
        Double secondcoord = firstNodeCoordinates[1];
        graphicsContext.setStroke(color);
        if (firstNodeCoordinates[0] > secondNodeCoordinates[0])
            firstcoord = secondNodeCoordinates[0];
        else sizeH = secondNodeCoordinates[0]-firstNodeCoordinates[0];
        if (firstNodeCoordinates[1] > secondNodeCoordinates[1])
            secondcoord = secondNodeCoordinates[1];
        else sizeV = secondNodeCoordinates[1]-firstNodeCoordinates[1];
        //graphicsContext.strokeRect(firstCoordinates[0], firstCoordinates[1], size, size);
        graphicsContext.strokeRect(firstcoord, secondcoord, sizeH, sizeV);
    }

    /**
     * Fill the rectangle.
     * @param graphicsContext
     */
    public void fill(GraphicsContext graphicsContext) {
        Double sizeH = firstNodeCoordinates[0] - secondNodeCoordinates[0];
        Double sizeV = firstNodeCoordinates[1] - secondNodeCoordinates[1];
        Double firstcoord = firstNodeCoordinates[0];
        Double secondcoord = firstNodeCoordinates[1];
        graphicsContext.setStroke(color);
        graphicsContext.setFill(color);
        if (firstNodeCoordinates[0] > secondNodeCoordinates[0])
            firstcoord = secondNodeCoordinates[0];
        else sizeH = secondNodeCoordinates[0]-firstNodeCoordinates[0];
        if (firstNodeCoordinates[1] > secondNodeCoordinates[1])
            secondcoord = secondNodeCoordinates[1];
        else sizeV = secondNodeCoordinates[1]-firstNodeCoordinates[1];
        graphicsContext.fillRect(firstcoord, secondcoord, sizeH, sizeV);
    }

    /**
     * @return table of size 4, 2 first elements are coordinates of the first node of the rectangle, 2 last one for the other node.
     */
    @Override
    public Double[] getCoordinates() {
        return new Double[]{firstNodeCoordinates[0], firstNodeCoordinates[1], secondNodeCoordinates[0], secondNodeCoordinates[1]};
    }

    /**
     * @return Get the TikZ code, with its sides and filling color set.
     */
    @Override
    public String getTikZCode() {
        Double[] coordinates = getCoordinates();
        return String.format("\\draw[color=%s,fill=%s] (%d,%d) rectangle (%d,%d);", color, color, coordinates[0].intValue(), coordinates[1].intValue(), coordinates[2].intValue(), coordinates[3].intValue());
    }

    /**
     * Calculates the x and y coordinates of 2 opposing nodes of the rectangle on the diagonal
     * using its length (we only make squares right now) and its center
     */
    public void determineNodes() {
        firstNodeCoordinates = new Double[]{firstCoordinates[0], firstCoordinates[1]};
        secondNodeCoordinates = new Double[]{firstCoordinates[0] + size, firstCoordinates[1] + size};
    }
}
