/*
package be.ac.ulb.infof307.g06.models.versioning;

import be.ac.ulb.infof307.g06.models.Database;
import javafx.util.Pair;
import java.util.ArrayList;
import java.util.Iterator;

 */

/**
 * Create a list with all the commits of a certain project.
 */

/*
public class CommitHistoryModel {

    private int projectID;
    private ArrayList<Pair<Integer, ArrayList<ArrayList<String>>>> commitHistory;
    /* List of all the branch of the projet, followed by a pair composed of the identifier of the branch, and his content.
     * The content of the branch is all his commits, which is an array.
     * This array of all the commits, contains an array of strings with all the infos about this commit.
     */

    /*
    private Database database;

    public CommitHistoryModel(int projectID) {
        this.projectID = projectID;
        createCommitHistory();
    }

    /**
     * Get the infos of all commits of all branches for one specific project.
     */

    /*
    private void createCommitHistory() {

        database = new Database();
        commitHistory = new ArrayList<>();
        ArrayList<Integer> allBranches = database.getBranches(projectID);
        Iterator<Integer> branchIterator = allBranches.iterator();
        while(branchIterator.hasNext()) {  //All branches
            Integer branchID = branchIterator.next();
            ArrayList<Integer> allCommits = database.getCommits(projectID, branchID);
            ArrayList<ArrayList<String>> thisBranchInfo = new ArrayList<>();       //Infos of this branch
            Iterator<Integer> commitIterator = allCommits.iterator();
            while (commitIterator.hasNext()) {   //All commits of a branch
                Integer commitID = commitIterator.next();
                ArrayList<String> thisCommitHistory = database.getCommitInfos(projectID, branchID, commitID);
                thisBranchInfo.add(thisCommitHistory);      //Add info of this commit
            }
            commitHistory.add(new Pair<>(branchID, thisBranchInfo));  //Add all infos of this branch
        }
    }
    /*

    /**
     * Get the infos of a specific branch of a project.
     * @param branchID branch number.
     * @return array with all the commits infos, if the branch doesn't exist, returns null.
     */
    /*
    public ArrayList<ArrayList<String>> getBranchInfo(Integer branchID) {
        Iterator<Pair<Integer, ArrayList<ArrayList<String>>>> projectIterator = commitHistory.iterator();
        while(projectIterator.hasNext()) {
            Pair<Integer, ArrayList<ArrayList<String>>> branch = projectIterator.next();
            if (branch.getKey() == branchID) { return branch.getValue(); }
        }
        return null;
    }
}
     */



