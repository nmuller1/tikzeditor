package be.ac.ulb.infof307.g06.models.versioning;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MetaDataReader {
    private String path;
    private File metaData;

    public MetaDataReader(String path) {
        this.path = path;
        metaData = new File(path);
    }

    /**
     * returns the content of a specified commit
     * @param ID : ID of the commit to find
     * @return : CommitModel which represents the commit with the correct ID
     * @throws IOException
     */
    public CommitModel getCommitFromID(String ID) throws IOException {
        String commitToStore = "";
        BufferedReader reader = new BufferedReader(new FileReader(path));
        String line = reader.readLine();
        while (line != null) {
            String[] lineToCheck = line.split(" ");
            if (lineToCheck[0].equals("commit")) {
                if (lineToCheck[1].equals(ID)) {
                    do {
                        commitToStore += line;
                        commitToStore += "\n";
                        line = reader.readLine();
                    } while (!line.substring(0, 6).equals("commit"));
                    break;
                }
            }
            line = reader.readLine();
        }
        reader.close();
        return new CommitModel(commitToStore);
    }
}
