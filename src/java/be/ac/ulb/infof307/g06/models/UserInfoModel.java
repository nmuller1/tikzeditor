package be.ac.ulb.infof307.g06.models;

/**
 * Class that contains the user info in static.
 */
public class UserInfoModel {
    private Integer userID = -1;
    private String username;

    public UserInfoModel(){
        userID = -1;
        username = "";
    }

    public UserInfoModel(Integer userID, String username){
        this.userID = userID;
        this.username = username;
    }
    public Integer getUserID() {
        return userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
