package be.ac.ulb.infof307.g06.models.importExport;

import be.ac.ulb.infof307.g06.models.UserInfoModel;

import java.io.File;
import java.io.IOException;

/**
 */
public class ImportModel {
    private UserInfoModel userInfoModel;
    private FolderDecompressionModel folderDeompressionModel;
    /**
     * Constructor
     */
    public ImportModel(UserInfoModel userInfoModel) {
        this.userInfoModel = userInfoModel;
        this.folderDeompressionModel = new FolderDecompressionModel(userInfoModel);
    }

    /**
     * UserInfoModel Getter
     * @return userInfoModel
     */
    public UserInfoModel getUserInfoModel() {
        return userInfoModel;
    }

    /**
     * Sets the value of userInfoModel
     * @param userInfoModel
     */
    public void setUserInfoModel(UserInfoModel userInfoModel) {
        if(userInfoModel.getUserID() > -2) {
            this.userInfoModel = userInfoModel;
        }
    }

    /**
     * Gets the ID of the owner of the project
     * @return userID
     */
    public Integer getUserInfoID() {
        return this.userInfoModel.getUserID();
    }

    /**
     * Gets the username of the owner of the project
     * @return username
     */
    public String getUserInfoUsername() {
        return this.userInfoModel.getUsername();
    }

    /**
     * First we get the destination path for the imported project,
     * Then We get the path including the filename and extension
     * of the imported file. Finally we decompress the tar.gz in the outputPath folder
     */
    public void importProject(File importedFile, String outputPath) throws IOException {
        String importedFileAbsolutePath = importedFile.getAbsolutePath();
        this.folderDeompressionModel.decompress(importedFileAbsolutePath, outputPath);
    }

    /**
     * Get the path where we need to save the files
     * by getting the path of the compressed folder
     */
    public static String getPathFromFile(File compressedFile){
        return compressedFile.getParent();
    }

}
