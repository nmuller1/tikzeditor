package be.ac.ulb.infof307.g06.models.drawing;

import java.util.ArrayList;

/**
 * Implements a stack used by the drawing view.
 */
public class ClickStack extends ArrayList<Double[]> {

    /**
     * Adds the value at the top op stack
     * @param value to be added
     */
    public void push(Double[] value) { add(value); }

    /**
     * Return and delete the value at the top
     * @return the value
     */
    public Double[] pop() {
        Double[] value = {};
        if (size() > 0) {
            value = remove(getTopOfStackIndex().intValue());
        }
        return value;
    }

    /**
     * @return the of top of stack index
     */
    private Integer getTopOfStackIndex() {
        return size() - 1;
    }
}
