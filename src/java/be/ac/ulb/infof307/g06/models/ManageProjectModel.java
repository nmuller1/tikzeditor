package be.ac.ulb.infof307.g06.models;

import be.ac.ulb.infof307.g06.utils.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Manage all the infos of the user displayed in the "ManageProjectView"
 */
public class ManageProjectModel {
    private static Database database;
    private List<FileInfo> userFileInfo;
    private final UserInfoModel userInfoModel;

    public ManageProjectModel(UserInfoModel userInfoModel) {
        this.userInfoModel = userInfoModel;
        updateUserFileInfo();
    }

    /**
     * Update the user file infos that will be displayed in the view table.
     */
    public void updateUserFileInfo() {
        database = new Database();
        List<Integer> idFilesOfUser = database.getProjectsOfUser(userInfoModel.getUserID());

        List<FileInfo> filesOfUser = new ArrayList<FileInfo>();
        for (int i = 0; i < idFilesOfUser.size(); i++) {
            String[] data = database.getProjectData(idFilesOfUser.get(i));
            File file = new File(data[1]);
            if (file.exists()) {
                filesOfUser.add(new FileInfo(data[0], userInfoModel.getUsername(), data[3], data[1], idFilesOfUser.get(i))); // this has to change when story 1 is completed
            } else {
                database.deleteProject(idFilesOfUser.get(i));
            }
        }
        userFileInfo = filesOfUser;
    }

    /**
     * Copy a file.
     * @param fileInfo
     */
    public void copyProject(FileInfo fileInfo) throws IOException{
        String oldName = fileInfo.getFilePath();
        File file = new File(oldName);
        File newFile = new File(copyNameFile(oldName)); // this provides the name for the copied file and creates it

        if (file.renameTo(newFile)) {
            System.out.println("Success");
        } else {
            throw new FileAlreadyExistsException("File already exist");
        }

        if (file.createNewFile()) {
            Database database = new Database();
            database.insertProject(userInfoModel.getUserID(), newFile.getName(), newFile.getAbsolutePath()); // this has to change when story 1 is completed
            //todo erase the System and eventually tell the user that project was copied via Pop up
            System.out.println("File Created");
        } else
            throw new FileAlreadyExistsException("File already exist");
        updateUserFileInfo();
    }

    /**
     * Delete a file in both database and user's computer
     * @param fileInfo
     */
    public void deleteProject(FileInfo fileInfo) {
        database.deleteProject(fileInfo.getProjectId());
        File file = new File(fileInfo.getFilePath());
        file.delete();
        updateUserFileInfo();
    }

    /**
     * Relocate a file in both database and user's computer.
     * @param fileInfo instance of the file project information
     * @param path
     */
    public void changeDirectory(FileInfo fileInfo, String path) throws FileNotFoundException {
        String os = System.getProperty("os.name");
        File file = new File(fileInfo.getFilePath());
        String separator = "\\";
        if(!os.equals("Windows 10")) separator = "/";
        if(path.indexOf("/") != -1) separator = "/";
        if(file.renameTo(new File(path+separator +fileInfo.getProjectName()))) {
            fileInfo.setFilePath(path+separator+fileInfo.getProjectName());
            file.delete();
            database = new Database();
            database.updatePath(fileInfo.getProjectId(),fileInfo.getFilePath());
        }
        else{
            throw new FileNotFoundException();
        }
    }

    /**
     * Rename a file in both database and user's computer
     * @param fileInfo
     */
    public boolean renameProject(String name, FileInfo fileInfo) {
        String oldName = fileInfo.getFilePath();
        Path path = Paths.get(oldName);
        String directory = path.getParent().toString();
        File oldFile = new File(oldName);
        File newFile = new File(directory + "/" + name);
        if (oldFile.exists()) {
            oldFile.renameTo(newFile);
            Database database = new Database();
            database.updatePath(fileInfo.getProjectId(), newFile.getAbsolutePath());
            database.renameProject(fileInfo.getProjectId(), newFile.getName());
            updateUserFileInfo();
            return true;
        } else {
            return false;
        }

    }

    /**
     * Connect to the edition window
     * @param fileInfo
     */
    public String editProject(FileInfo fileInfo) throws IOException {
        return FileUtils.fileToString(fileInfo.getFilePath());
    }


    /**
     * Getter that returns a copy of the list of the files information of the user.
     * @return List of FileInfo copy of the List userFileInfo.
     */
    public List<FileInfo> getUserFileInfo() {
        List<FileInfo> listCopy = new ArrayList<FileInfo>(userFileInfo);
        return listCopy;
    }

    /**
     * this constructs a correct file name with "_copy" paying attention to extension of files
     * @param fileName
     * @return file name with "_copy" at the right spot
     */
    public String copyNameFile(String fileName){
        // TODO : pass this method in protected and change TestManageProjectModel; no time left for it.
        int dotIndex = fileName.lastIndexOf(".");
        if (dotIndex == -1) {
            return fileName + "_copy";
        }
        else {
            String beforeDot = fileName.substring(0,dotIndex);
            String afterDot = fileName.substring(dotIndex);
            return beforeDot + "_copy" + afterDot;
        }
    }
}
