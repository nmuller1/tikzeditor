package be.ac.ulb.infof307.g06.models.importExport;

import java.io.*;
import be.ac.ulb.infof307.g06.models.Database;
import be.ac.ulb.infof307.g06.models.UserInfoModel;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.utils.IOUtils;

/**
 * Class for decompression of tar.gz files
 */
public class FolderDecompressionModel {
    private final Database database;
    private final UserInfoModel userInfoModel;
    private TarArchiveInputStream tarArchiveInputStream;

    public FolderDecompressionModel(UserInfoModel userInfoModel) {
        database = new Database();
        this.userInfoModel = userInfoModel;
    }

    /**
     * Decompresses a tar.gz file to the directory whose path is the outputPath specified
     * @param tarFileName : path to the tar.gz
     * @param outputPath : destination of the extracted files
     */
    public void decompress(String tarFileName, String outputPath) {
        File outputDirectory = new File(outputPath);
        try {
            tarArchiveInputStream = new TarArchiveInputStream(new GzipCompressorInputStream(new FileInputStream(tarFileName)));
            TarArchiveEntry entry;
            while ((entry = tarArchiveInputStream.getNextTarEntry()) != null) {
                if (!entry.isDirectory()) {
                    extractTarEntry(entry, outputDirectory);
                }
            }
            tarArchiveInputStream.close();
        } catch (Exception e) {
            System.err.println("An error occurred while trying to access the archive:");
            e.printStackTrace();
        }
    }

    /**
     * Tries to extract all the files contained in the given .tar archive entry
     * @param entry of the .tar archive
     * @param outputDirectory to extract the filers
     */
    private void extractTarEntry(TarArchiveEntry entry, File outputDirectory) {
        if (!entry.isDirectory()) {
            File currentFile = new File(outputDirectory, entry.getName());
            database.insertProject(this.userInfoModel.getUserID(), currentFile.getName(), currentFile.getAbsolutePath());
            File parent = currentFile.getParentFile();
            if (!parent.exists() && !parent.mkdirs()) {
                System.err.println("An error occured while trying to reach the given output path: " + outputDirectory.getAbsolutePath());
            }
            try {
                FileOutputStream output = new FileOutputStream(currentFile);
                IOUtils.copy(tarArchiveInputStream, output);
                output.close();
            } catch (Exception e){
                System.err.println("An error occurred while trying to decompress a file:");
                e.printStackTrace();
            }
        }
    }
}
