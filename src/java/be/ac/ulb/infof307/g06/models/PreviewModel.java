package be.ac.ulb.infof307.g06.models;

import be.ac.ulb.infof307.g06.controllers.drawing.DrawingController;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.WritableImage;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import javax.imageio.ImageIO;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * PDF file preview Model
 */
public class PreviewModel {
    private final DrawingController drawingController;
    private final Canvas diagramCanvas;

    /**
     * Constructor of the model
     */
    public PreviewModel(DrawingController drawingController){
        this.drawingController = drawingController;
        this.diagramCanvas = this.drawingController.getCanvas();
    }

    public Canvas getDiagramCanvas() { return diagramCanvas; }

    /**
     * Makes a PNG image of the current Canvas object with the diagram
     * @return File image PNG
     */
    public File getCanvasImage() throws IOException{
        File imageOutput = null;
        WritableImage writableImage = new WritableImage(800, 800);
        this.diagramCanvas.snapshot(null, writableImage);
        RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
        imageOutput = new File("canvas" + new Date().getTime() + ".png");
        ImageIO.write(renderedImage, "png", imageOutput);
        //todo refere to the todo below
        /*
        Logger.getLogger(PreviewModel.class.getName()).log(Level.SEVERE, null, ex);*/
        return imageOutput;
    }

    /**
     * Generate a pdf with the text and the canvas image
     * @return String filename the path of the generated PDF
     */
    public String generateFullPDF() throws IOException {
        String filename = "";
        File canvasImage = this.getCanvasImage();
        PDDocument document = new PDDocument();
        PDPage imagePage = new PDPage();
        PDImageXObject pdfImage;
        PDPageContentStream content;
        pdfImage = PDImageXObject.createFromFile(canvasImage.getAbsolutePath(), document);
        content = new PDPageContentStream(document, imagePage);
        float scale = 0.7f;
        content.drawImage(pdfImage, 20, 200, pdfImage.getWidth() * scale, pdfImage.getHeight() * scale);
        content.close();
        document.addPage(imagePage);
        filename = "diagramPreview.pdf";
        document.save(filename);
        document.close();
        //todo i don't have an idea what this does so i leave it here while handling my own problems
        /*Logger.getLogger(PreviewModel.class.getName()).log(Level.SEVERE, null, ex);*/
        canvasImage.delete();
        return filename;
    }
}
