package be.ac.ulb.infof307.g06.models;


/**
 * Manage the account and link it to the database.
 */
public class AccountModel {
    Database database;

    public AccountModel() {
        database = new Database();
    }

    /**
     * create a new user inside the database from the given parameters
     * check that both username and email are available before signing
     * it up into the database.
     * @param username
     * @param lastName
     * @param firstName
     * @param email
     * @param password
     * @return id of the inserted user ; -1 if error occured == cant sign up the user
     */
    public int signUp(String username, String lastName, String firstName, String email, String password) {
        if (database.isEmailAvailable(email) && database.isUsernameAvailable(username)) {
            return database.createUser(username, lastName, firstName, email, password);
        }
        return -1;
    }

    /**
     * sign in a user by checking username and password match
     * @param username
     * @param password
     * @return user's id ; -1 if error occured == cant sign in the user
     */
    public int signIn(String username, String password) {
        int id = database.getUserID(username);
        if (id != -1) {
            String[] data = database.getInfoUser(id);
            if (!data[4].equals(password)) {
                return -1;
            }
        }
        return id;
    }

    /**
     * modify the username of the specified user
     * @param id of the user.
     * @param newUsername username that will replace the old one.
     * @return message that will be displayed if there is an error.
     */
    public String updateUsername(int id, String newUsername) {
        String message = "";
        if (!newUsername.isEmpty()) {
            if (database.isUsernameAvailable(newUsername)) {
                if (verifyUsername(newUsername)) {
                    database.modifyUsername(id, newUsername);
                } else { message = "Please enter a valid username (without special characters and upper cases)."; }
            } else { message = "Username not available."; }
        } else { message = "Please enter a new username."; }
        return message;
    }

    /**
     * Check if username contains only lower cases (at least one letter) and no special characters.
     * @param username that will be verified.
     * @return true is username is valid, false otherwise.
     */
    private boolean verifyUsername(String username) {
        return username.matches("[a-z]+");
    }

    /**
     * modify the email of the specified user
     * @param id of the user.
     * @param newEmail email that will replace the old one.
     * @return message that will be displayed if there is an error.
     */
    public String updateEmail(int id, String newEmail) {
        String message = "";
        if (!newEmail.isEmpty()) {
            if (verifyEmail(newEmail)) {
                if (database.isEmailAvailable(newEmail)) {
                    database.modifyEmail(id, newEmail);
                } else { message = "Email not available."; }
            } else { message = "The email is not valid."; }
        } else { message = "Please enter a new email."; }
        return message;
    }

    /**
     * Verify if the mail is in a correct format.
     * @param email string of the email.
     * @return true if email is valid, false if not.
     */
    private boolean verifyEmail(String email) {
        int firstAt = email.indexOf('@');
        if (firstAt == -1 || firstAt == 0)
            return false;
        int secondAt = email.lastIndexOf('@');
        if (firstAt != secondAt)
            return false;
        int dot = email.lastIndexOf('.');
        if (dot == -1 || dot < firstAt || dot == firstAt + 1)
            return false;
        String domain = email.substring(dot + 1);
        String[] domains = {"be", "com", "fr", "lu", "org", "eu", "nl", "de", "edu"};
        for (String dom : domains) {
            if (domain.toLowerCase().equals(dom))
                return true;
        }
        return false;
    }

    /**
     * modify the password of the specified user
     * @param id of the user.
     * @param oldPassword current password that will be changed.
     * @param newPasswordOne new password.
     * @param newPasswordTwo confirmation of the new password.
     * @return message that will be displayed if there is an error.
     */
    public String updatePassword(int id, String oldPassword, String newPasswordOne, String newPasswordTwo) {
        String message = "";
        String[] data = database.getInfoUser(id);
        if (!oldPassword.isEmpty() && oldPassword.equals(data[4])) { //oldPassword is correct
            if (!newPasswordOne.isEmpty()) {
                if (!newPasswordTwo.isEmpty()) {
                    if (newPasswordOne.equals(newPasswordTwo)) {
                        if (newPasswordOne.length() >= 8) {
                            database.modifyPassword(id, newPasswordOne);
                        } else { message = "Please enter a password of minimum 8 characters."; }
                    } else { message = "Please enter the same password to confirm."; }
                } else { message = "Please confirm your new password."; }
            } else { message = "Please enter a new password."; }
        } else { message = "Your old password is incorrect."; }
        return message;
    }
}
