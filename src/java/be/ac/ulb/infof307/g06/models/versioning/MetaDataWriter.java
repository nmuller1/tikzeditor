package be.ac.ulb.infof307.g06.models.versioning;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class MetaDataWriter {
    private File metaData;

    /**
     * constructor protected for the test
     */
    protected MetaDataWriter() {
        metaData = null;
    }

    public MetaDataWriter(String path) {
        metaData = new File(path);
    }

    /**
     * write commit in metadata file
     * @param commit : commit to write
     * @throws IOException
     */
    public void writeCommit(String commit) throws IOException {
        if (verifyCommit(commit)) {
            FileWriter writer = new FileWriter(metaData, true);
            if (metaData.length() == 0) writer.write(commit);
            else writer.write("\n"+commit);
            writer.close();
        }
        else {
            throw new IOException();   //TODO : Better type of exception
        }
    }

    /**
     * verify if the commit is in the coorrect format
     * @param commit : commit to verify the format
     * @return true if format is correct, false otherwise
     */
    protected boolean verifyCommit(String commit) {
        boolean res = true;
        boolean flagAdd = true;
        boolean flagRemove = false;
        int i = 1;
        String[] commitToVerify = commit.split("\n");
        String[] firstline = commitToVerify[0].split(" ");
        if (!firstline[0].equals("commit")) res = false;
        try {
            if (!firstline[1].matches("[0-9]+")) res = false;
        }
        catch (IndexOutOfBoundsException e) {
            res = false;
            i = commitToVerify.length;
        }
        while (commitToVerify.length > i) {
            if (commitToVerify[i].indexOf('\t') != 0) {
                res = false;
                i = commitToVerify.length;
            }
            else {
                char addOrRemove = commitToVerify[i].charAt(1);
                if (addOrRemove == '-' && flagRemove == false) {
                    flagRemove = true;
                    flagAdd = false;
                }
                else if ((addOrRemove != '+' && addOrRemove != '-') || (addOrRemove == '+' && !flagAdd) || (addOrRemove == '-' && flagAdd)) {
                    res = false;
                    i = commitToVerify.length;
                }
                i++;
            }
        }
        return res;
    }


    /**
     * verify that a branch, merge or commit statement is in the correct format
     * @param statement : the Statement to verify the format (branch, merge or revert)
     * @return true if the statement is in the correct format, false otherwise
     */
    protected boolean verifyStatement(String statement) {
        boolean res = true;
        String[] branchToVerify = statement.split(" ");
        if (branchToVerify.length != 4) res = false;
        else {
            if (branchToVerify[0].equals("branched")) {
                if (!branchToVerify[2].equals("from")) res = false;
            }
            else if (branchToVerify[0].equals("merged") || branchToVerify[0].equals("reverted")) {
                if (!branchToVerify[2].equals("to")) res = false;
                if (branchToVerify[0].equals("reverted") &&
                        !branchToVerify[3].substring(0, branchToVerify[3].length()-1).matches("[0-9]+")) {
                    res = false;
                }
            }
            else res = false;
            if (statement.charAt(statement.length()-1) != ';') {
                res = false;
            }
        }
        return res;
    }


}
