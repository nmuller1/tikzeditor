package be.ac.ulb.infof307.g06.models;

/**
 * Class that implements the metadata of a given file.
 */
public class FileInfo {
    private String filePath;
    private String projectName;
    private String ownerName;
    private String lastModification;
    private int projectId;

    public FileInfo() {
    }

    /**
     * Constructor of FileInfo class.
     * @param projectName      string of the project name.
     * @param ownerName        string of the name of the owner.
     * @param lastModification string of the timestamp of the last modification.
     * @param filePath         string of the absolute file path.
     */
    public FileInfo(String projectName, String ownerName, String lastModification, String filePath, int projectId) {
        this.projectName = projectName;
        this.lastModification = lastModification;
        this.ownerName = ownerName;
        this.filePath = filePath;
        this.projectId = projectId;
    }

    /**
     * Getter to get the project name.
     * @return string of the project name.
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * Setter to set the project name.
     * @param projectName string the new project name.
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /**
     * Getter to get the name of the owner of the given file.
     * @return string of the name of the given file.
     */
    public String getOwnerName() {
        return ownerName;
    }

    /**
     * Setter to set owner name of a given file.
     * @param ownerName string of the new owner a given file.
     */
    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    /**
     * Getter to get the timestamp of the last modification of a given file.
     * @return string the timestamp of the last modification of a given file.
     */
    public String getLastModification() {
        return lastModification;
    }

    /**
     * Setter to set le timestamp of the last modification of a file.
     * @param lastModification string of the new timestamp of the last modification of a given file.
     */
    public void setLastModification(String lastModification) {
        this.lastModification = lastModification;
    }

    /**
     * Getter to get the absolute file path of a given file.
     * @return string of the absolute file path of a given file.
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Setter to set the file path of a given file.
     * @param filePath string of the new absolute file path of a given file.
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Getter to get the project ID.
     * @return int the project ID.
     */
    public int getProjectId() {
        return projectId;
    }

    /**
     * Setter to set the project ID.
     * @param projectId string of the new project ID.
     */
    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }
}