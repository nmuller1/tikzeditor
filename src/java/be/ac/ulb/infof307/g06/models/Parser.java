package be.ac.ulb.infof307.g06.models;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Parse a text to add the syntax coloration.
 */
public class Parser {
    static String text;
    static ArrayList<String> definedObjects = new ArrayList<String>();
    static Integer cursorChar = 0;

    /**
     * Constructor
     */
    public Parser() {
        text = "";
        cursorChar = text.indexOf("\\");
    }

    /**
     * Reset the position of cursor
     */
    public void goToBegin() {
        cursorChar = 0;
        cursorChar = text.indexOf("\\");
    }

    /**
     * Extract the TikZ main instruction
     * @param instruction is the full line
     * @return the elementary instruction
     */
    public String getElementaryInstruction(String instruction) {
        if (instruction.indexOf('[') == -1) {
            return instruction.substring(1, instruction.indexOf(" "));
        } else {
            return instruction.substring(1, instruction.indexOf("["));
        }
    }

    /**
     * Extract the TikZ options (in [])
     * @param instruction is the full line
     * @return a readable array with parsed options
     */
    public String[] getInstructionOptions(String instruction) {
        // We consider that only color, thickness and dashing are available (for the moment)
        String[] res = new String[3];
        res[0] = "black";
        res[1] = "30";
        res[2] = "";
        if (instruction.indexOf('[') != -1) {
            String options = instruction.substring(instruction.indexOf('[') + 1, instruction.indexOf(']'));
            String[] temporary = options.split(",");
            for (int i = 0; i < temporary.length; i++) {
                if (temporary[i].substring(0, 5).equals("color")) {
                    res[0] = temporary[i].split("=")[1];
                    break;
                }
            }
            if (Arrays.asList(temporary).contains("dash")) {
                res[2] = "1";
            }
            if (Arrays.asList(temporary).contains("->")) {
                res[2] = "2";
            }
            for (int i = 0; i < temporary.length; i++) {
                try{
                    res[1] = String.valueOf(Integer.parseInt(temporary[i]));
                } catch (NumberFormatException e){
                    //todo Need to handle this error.
                }

            }
        }
        return res;
    }

    /**
     * Extract the way to link the dots
     * @param instruction is the full line
     * @return the part of the line specifying the link (--, circle or rectangle)
     */
    public String getLinkInInstruction(String instruction) {
        return (instruction.substring(instruction.indexOf(")") + 1, instruction.indexOf(')') + instruction.substring(instruction.indexOf(")")).indexOf("("))).trim();
    }

    /**
     * Extract the coordinates. In case of circle, the first tuple is the center, and the second is the radius and zero
     * @param instruction is the full line
     * @return a 2x2 matrix with coordinates
     */
    public Double[][] getCoordinates(String instruction) {
        String[] parsedCoordinates = instruction.split(getLinkInInstruction(instruction));
        Double[][] result = new Double[parsedCoordinates.length][2];
        String[] firstCoordinate = parsedCoordinates[0].substring(parsedCoordinates[0].indexOf('(') + 1, parsedCoordinates[0].indexOf(')')).split(",");
        result[0][0] = Double.parseDouble(firstCoordinate[0]);
        result[0][1] = Double.parseDouble(firstCoordinate[1]);
        if (!getLinkInInstruction(instruction).equals("circle")) {
            for (int i = 1; i < parsedCoordinates.length; ++i) {
                String[] tmp_coord = parsedCoordinates[i].substring(parsedCoordinates[i].indexOf('(') + 1, parsedCoordinates[i].indexOf(')')).split(",");
                result[i][0] = Double.parseDouble(tmp_coord[0]);
                result[i][1] = Double.parseDouble(tmp_coord[1]);
            }
        } else {
            String secondCoordinate = parsedCoordinates[1].substring(parsedCoordinates[1].indexOf('(') + 1, parsedCoordinates[1].indexOf('p'));
            result[1][0] = Double.parseDouble(secondCoordinate);
            result[1][1] = 0.0;
        }
        return result;
    }

    /**
     * Goes through the text extracting every instruction,
     */
    private void parseText() {
        String instruction = getNextInstruction();
        while (!instruction.equals("0")) {
            instruction = getNextInstruction();
        }
    }

    //Getters and Setters.
    public static String getText() { return text; }
    public void setText(String newText) {
        text = newText;
        cursorChar = text.indexOf("\\");
    }

    /**
     * Sets the cursor at the begin of the next instruction in text.
     * @return the instruction just red
     */
    public String getNextInstruction() {
        if (cursorChar != -1 && cursorChar < text.length()) { // -1: no \ found, text.length(): end of text
            Integer endOfInstructionIndex = cursorChar + text.substring(cursorChar).indexOf(";\n") - 1;
            if (endOfInstructionIndex > 0 || (cursorChar + text.substring(cursorChar).indexOf(";")) == text.length() - 1) {
                if ((cursorChar + text.substring(cursorChar).indexOf(";")) == text.length() - 1) {
                    endOfInstructionIndex = text.length() - 2;
                }
                String instruction = text.substring(cursorChar, endOfInstructionIndex + 1);
                if ((text.substring(endOfInstructionIndex)).indexOf('\\') != -1) { // if a \ is found
                    cursorChar = (text.substring(endOfInstructionIndex)).indexOf('\\') + endOfInstructionIndex;
                } else {
                    cursorChar = -1;
                }
                return instruction;
            } else {
                return "0";
            }
        } else {
            return "0";
        }
    }
}