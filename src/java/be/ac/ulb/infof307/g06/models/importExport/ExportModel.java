package be.ac.ulb.infof307.g06.models.importExport;

import java.io.File;
import java.io.IOException;

/**
 * Export a file and compress it.
 */
public final class ExportModel {
    public static void exportProject(String exportPath, File projectFile) throws IOException {
        FolderCompressionModel.compress(exportPath, projectFile);
    }
}
