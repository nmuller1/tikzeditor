package be.ac.ulb.infof307.g06.models;

import java.io.File;
import java.io.IOException;

public class CreateProjectModel {
    private Database database;
    private UserInfoModel userInfoModel;

    public CreateProjectModel(UserInfoModel userInfoModel){
        this.database = new Database();
        this.userInfoModel = userInfoModel;
    }

    /**
     * Inserts new project in database and returns project ID of new file
     * @return project ID
     * @throws IOException in case of opening failure
     */
    public int createNewProject(File projectFile) throws IOException {
        int projectID = database.insertProject(userInfoModel.getUserID(), projectFile.getName(), projectFile.getAbsolutePath());
        return projectID;
    }
}