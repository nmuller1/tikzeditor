package be.ac.ulb.infof307.g06.utils;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * class StringComparator contains static methods to get the differences between strings
 */
public class StringComparator {
    /**
     * Get all the insertions that occurred in newText compared to originalText
     * @param originalText : the first string
     * @param newText : the new string that we will be comparing to the original one
     * @return an ArrayList with inserted line in newText
     */
    public static ArrayList<String> getInsertions(String originalText, String newText) {
        ArrayList<String> originalTextArray = splitIntoArrayList(originalText);
        ArrayList<String> newTextArray = splitIntoArrayList(newText);
        for(String character : originalTextArray){
            newTextArray.remove(character);
        }
        return newTextArray;
    }

    /**
     * Get all the deletions that occurred in newText compared to originalText
     * @param originalText : the first string
     * @param newText : the new string that we will be comparing to the original one
     * @return an ArrayList with deleted line in newText
     */
    public static ArrayList<String> getDeletions(String originalText, String newText) {
        ArrayList<String> originalTextArray = splitIntoArrayList(originalText);
        ArrayList<String> newTextArray = splitIntoArrayList(newText);
        for(String character : newTextArray){
            originalTextArray.remove(character);
        }
        return originalTextArray;
    }
    /** Function used to split each line into an ArrayList containing them
     * @param text : the string to split into an arraylist
     * @return an arraylist<String> containing each line of the text
     */
    protected static  ArrayList<String> splitIntoArrayList(String text){
        return new ArrayList<>(Arrays.asList(text.split("\n")));
    }

    /**
     * Get all the common lines between newText and originalText
     * @param originalText : the first string
     * @param newText : the new string that we will be comparing to the original one
     * @return an ArrayList with common lines
     */
    public static ArrayList<String> getCommon(String originalText, String newText) {
        ArrayList<String> originalTextArray = splitIntoArrayList(originalText);
        ArrayList<String> newTextArray = splitIntoArrayList(newText);
        ArrayList<String> result = new ArrayList<>();
        for(String line : newTextArray){
            if(originalTextArray.contains(line) &&
                    getNumberOfOccurrence(line,originalTextArray) != getNumberOfOccurrence(line,result)&&
                    getNumberOfOccurrence(line,newTextArray) != getNumberOfOccurrence(line,result)
            ) result.add(line);
        }
        return  result;
    }

    /** Count the number occurence of a string in an arrayList
     * @param stringToCount : the string to count
     * @param arrayToParse : the array to parse
     * @return res : an int containing the number of occurrences
     */
    protected static int getNumberOfOccurrence(String stringToCount, ArrayList<String> arrayToParse){
        int res = 0;
        for(String line : arrayToParse){
            if(line.equals(stringToCount)) res++;
        }
        return res;
    }

}
